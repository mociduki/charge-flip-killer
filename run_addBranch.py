#!/bin/usr/python

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys= str(os.environ['ROOTSYS'])

gROOT.ProcessLine(".L AddBranch.C+")

#inputPath="trainingNtuple/tree_Zee_ALL.root"
inputPath="trainingNtuple/tree_data_ALL.root"
trainName="BDT_pid1_etaBin0_p2b_mlBDT_bG_t403_d5_c200_m0p5_0x1f010"

chflipAna= AddBranch(inputPath, trainName)
chflipAna.Loop();

print "EndOfRun"
