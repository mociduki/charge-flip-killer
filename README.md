Charge Flip Killer
===
A training package of the charge flip tagger (CFT).
It comprises many codes to allow you to do the following items:
- ntuple-transform from event level to the electron level
- actual training code
- evaluation after the training

Main codes
---
* ntupleTransformer.C  
   It makes mini-ntuples for BDT trainings for charge mis-ID reduction.

<!--
Also, it produces histograms to study features of mis-ID and good-ID electrons
with all variables contained in ntuples, but this feature isn't the main usage.
-->

* TMVAClassification.C  
   Perform training(s) of Charge Flip Killer BDT.

Preparation
---
To download/checkout the package, type the following command  
`# git clone https://gitlab.cern.ch/mociduki/charge-flip-killer ChargeFlipKiller`  
Note that the last argument `ChargeFlipKiller`  can be changed by your preference, e.g. ChargeFlipKiller_20160730.

<!--
Just to keep this old command for SVN
# svn co svn+ssh://svn.cern.ch/reps/atlas-mociduki/egamma/ChargeFlipKiller/trunk ChargeFlipKiller
-->

To setup the necessary software,  
`  # cd ChargeFlipKiller #chdir to your package downloaded, e.g. ChargeFlipKiller_20160730`  
`  # . setup.sh`

- Input Sample  
  The samples used are Z->ee MC ntuples made by Kristin Lowhasser (e/gamma expert) with Powheg+Pythia8 generators. The ntuple dumper is called TagAndProbeFrame and described at the twiki link:  
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronTagAndProbeFrame  
  To run on different (new) samples, you might have to modify ntupleTransformer.h, starting around line 688.

To run ntuple transformation
---
- To run the code (Simplified way)  
 You can run the code directly from the prompt command by typing:
`    # python run_ntupleTransformer.py`  
 Check the python file above and change some part, if necessary. E.g. inputPath etc. The output file is placed in a directory called `trainingNtuple/`.

- To run the code (Montreal-specific)  
  This part is dedicated to run the code in atlasXX servers (XX=11,12,13). The instructions to run the code using the qsub command are in the submit_job.sh file. You can type the following command for example:  
  `# qsub -N miniNtuplesBDT submit_job.sh`  
  It will produce two .root files, one for TIGHT electrons and one for MEDIUM electrons for each eta bins (8 files in total).

- Output trees  
 The input is an event level ntuple and the output is an electron level ntuple for the BDT trainings. Tree0 contains electrons from events with even event number (event_number%2==0), while Tree1 contains electrons from events with odd  event number (event_number%2==1). Signal electrons (goodID) and background electrons (misID) are stored in those trees but applying a cut "isFlip==0" can classify signals, and vice versa. All the input variables for the BDT training should be stored in this output ntuple.
<!--
NB: See at the very bottom of this file for the other output, which are many histograms. It's skipped here as it's not the main point.
-->

To run the training process
---
The actual training specifications are written in `TMVAClassification.C`.  The so-called cross validation (also called as k-fold, k=2 in this use case) method is used in order to keep full statistics for the application phase, and to crosscheck the training performance (overtrainings etc) in the 2-subdevided samples. As described above, the sub-division is based on the event number to be even, or odd (event_number%2==0 or 1). To run such 2 trainings at once (***now only single training, x-validation is off by default***), you need the following command
`    # python run_xTrainings.py weightmethod bdtoption pid HexNumber #see below for weightmethod, bdtoption, nfold, pid, and HexNumber`
This will create 2 directories (train0, and train1) and submit 2 independent trainings in these directories. The script will wait for both trainings to be done, and will merge the training output files. The evaluation is also performed by this script with the combined file. To see what it exactly does for the evaluation, see eval.C.

##### The arguments
+ weightmethod argument  
  This is to specify the weighting method is used for the trainings. The options are the followings:  
  1. pe2s: pteta2sig: (pt,eta) to signal
  1. pe2b: pteta2bkg: (pt,eta) to background
  1. e2s : eta2sig  : (eta)    to signal
  1. e2b : eta2bkg  : (eta)    to background
  1. pef : ptetaflat: (pt,eta) is flattened
  1. ef  : etaflat  : (eta) is flattened
  To not apply the reweighting at all, give `none` to this argument.

+ mva argument:  
  mlBDT or mlDNN is used to decide MVA method for the training.

+ dnnoption argument:  
  This is to specify the dnn hyperparameters for the training.
  It should look like `mlDNN_lo10n20n10_lr0.1_mo0.9_ep20_cs100_bs1000_dc0p0.5p0.5p0`  
   - lo stands for layout           : layout of the network
   - lr stands for learning rate    :
   - mo stands for momentum         :
   - ep stands for epoch            : number of epochs/repetition
   - cs stands for convergence steps:
   - bs stands for batch size       :
   - wd stands for weight decay     :
   - rg stands for regularization   :
   - tr stands for test repetition  :
   - dc stands for drop config      :

+ bdtoption argument:  
  This is to specify the bdt hyperparameters for the training. It should look like `mlBDT_bG_t300_d10_c200_m0p5`.  

	| option   | meaning          | description |
	| -------- | ---------------- | ------------- |
	| bG       | boost type       | yype of the boosting scheme |
	| t        | tree             | number of trees in the BDT forest |
	| d        | depth            | number of depth in a single tree |
	| c        | cut              | number of cuts used to optimize a cut at each node |
	| m        | MNS              | minimum node size. The unit is %, and p for '.' When the split reaches to a certain fraction of the entire sample, the tree stops to grow at the node and labels sig or bkg |

+ nfold argument: *this is not currently in use though..*  
  This is to specify how many (folding) parts you'd like to cut the entire sample into. In the example above, the cross validation is explained with nfold==2, but the code is also adapted to nfold: if you give 3 to this argument, your dataset is divided into 3 and only one will be used for training and the rest for training.  You can give any positive integer (>1), up to the number of events in the entire sample.

+ pid  argument  
   pid argument should be given by int [0-3]. The training is done using electrons selected by following PID menu.
     * 0: No PID applied i.e. all reconstructed electrons
     * 1: Loose  LH PID applied
     * 2: Medium LH PID applied
     * 3: Tight  LH PID applied

+ HexNumber argument  
   this will be used for the switch for the variables.  
   `0x07e00` is (207 in decimals)  
   `0000 0000 0000 1100 1111` in binary.  
   Each digit works as a switch of the corresponding variable (see TMVAClassification.C for details). The default configuration is 0x07e00 which uses default 6 variables.

 To visualize each training and testing results, start root and type following command.
    root [0] TMVA::TMVAGui("train0/weights/TMVA_yourTrainingName.root")
 This will pop up the usual TMVAGui, go ahead clicking them.

Plotting macro
---
After training and testing have been finished, you can further visualize the results, especially for the deferential performance (as a function of pt/eta), using plot.C.
The usage looks like below within the root interpreter:  
`   > .L plot.C+`  
`   > plot("inputGraphFileName_graphs.root", 95, 0, 1)`  
Here are the arguments:  
* inputFileName  
  input file should be the output from the previous run, which is dumped under `postprocess/`. The file already contains graphs and this macro just plot in a fancy way.
* 95  
  This is the Operating point. The cut applied on the MVA discriminant is to keep 95% signal electrons inclusively in pt/eta.
* 0  
  This is a curve type option (integer), which determines type of curve to be drawn.  
     - The global efficiency means inclusive efficiency in all pt/eta.
     - The flat efficiency means efficiency in a given bin  
       Efficiency in all the pt/eta bins are fixed at 95%, for instance. And therefore the cut applied on the discriminant in each bin is different.

	| No. | abreviation     | description  |
	| --- | --------------  | -------------------------------  |
	| 0   | rejfromcutvspt  | global efficiency plot Rej vs pt |
	| 1   | rejvspt         | flat   efficiency plot Rej vs pt |
	| 2   | rejvseff        | overall rejection vs efficiency plot |
	| 3   | rejfromcutvseta | global efficiency plot Rej vs eta |
	| 4   | rejvseta        | flat   efficiency plot Rej vs eta |
	| 5   | effvspt         | global cut eff vs pt |
	| 6   | effvseta        | global cut eff vs eta |
* 1  
  Ratio option. When this boolean is true, it will draw a ratio to the first entry of the graph on the lower pannel.  

Additional info (usually not need)
---
##### Output histograms and trees
  Output of the ntupleTransformer contains many histograms, in addition to 2 trees: a set of histograms for each variables contained in the input samples. These histograms are now stored in the sub-directory in the root file, called "histograms". Each distribution is separated in 4 different contribution:

   1. misID  and type==2 (prompt)    electrons
   1. misID  and type==4 (Conv+Brem) electrons
   1. wellID and type==2 (prompt)    electrons
   1. wellID and type==4 (Conv+Brem) electrons

These output histograms can be drawn in a single plot using a macro called histo_TYPES.C from the 4 contributions on the same plot, set the axis name, legend, etc. The instructions to run it are at the beginning of the macro.

Misc
---
Any question is welcomed to Kazuya.Mochizuki@cern.ch

##### Authors:
* Kazuya Mochizuki, Post-doc. trainee @ U. Montreal (Physically@CERN)
* Hubert Trepanier, MSc. student      @ U. Montreal
* Jerome Claude   , MSc. student      @ U. Montreal

> Written with [StackEdit](https://stackedit.io/).