#include <TROOT.h>
#include <map>
#include <iostream>
#include <TMath.h>
#include <TF1.h>
 
// The name of all variables defined in Utilities.h start with "_"

const int _nChan = 3;
TString _channels[_nChan] = {"mumu","emu","ee"};

const int _nLepFlav = 2;
TString _lepFlav[_nChan] = {"mu","e"};

const int _nRegions_mumu = 7;
TString _regions_mumu[_nRegions_mumu] = {"2iso_SS","2iso_OS","d0sign3_nod0_SS","isoInt_SS","intd0sign3_nod0_SS","iso_isoInt_SS","isoInt_iso_SS",};
const int _nRegions_emu = 12;
TString _regions_emu[_nRegions_emu] = {"2iso_SS","2iso_OS","2iso_SS_etight","2iso_OS_etight","e_isoInt_SS","mu_isoInt_SS","e_failmedpp_SS","mu_d0sign3_nod0_SS","etight_isoInt_SS","etight_isoInt_OS","mu_isoInt_etight_SS","mu_isoInt_etight_OS"};
const int _nRegions_ee = 14;
TString _regions_ee[_nRegions_ee] = {"signal","2iso_OS","2iso_SS_Z","SS_med_interIso","SS_med_bothInterIso","SS_failmedpp","SS_bothFailmedpp","SS_medpp_interIso","SS_medpp_bothInterIso","SS_tightpp_interIso","SS_tightpp_bothInterIso","signal_tight","2iso_OS_tight","2iso_SS_Z_tight"};

const int _nLepPairTypes = 4;
TString _lep_pair_type[_nLepPairTypes] = {"NN","ND","DN","DD"};

const int _nLepType_mu = 8;
TString _lepType_mu[_nLepType_mu] = {"n_iso","n_d0sign3_nod0","n_isoInt","n_intd0sign3_nod0",
				     "d_iso","d_d0sign3_nod0","d_isoInt","d_intd0sign3_nod0"};
const int _nLepType_e = 14;
TString _lepType_e[_nLepType_e] = {"n_med","n_medpp","n_tightpp","n_med_interiso","n_medpp_interiso","n_tightpp_interiso","n_med_failmedpp", // electrons numerator
				   "d_med","d_medpp","d_tightpp","d_med_interiso","d_medpp_interiso","d_tightpp_interiso","d_med_failmedpp"};// electrons denominator


const int _nSyst = 27;
TString _systType[_nSyst] = {
  "", //no systematic
  "_e_fake_up","_e_fake_dn","_mu_fake_up","_mu_fake_dn","_bothemu_fake_up","_bothemu_fake_dn", //fake systematics 
  "_e_MV1fake", "_e_MV1fake_up", "_e_MV1fake_dn", //electron fakes derived with MV1 instead
  "_WZup", "_WZdn", //varying WZ k-factor  
  "_mu_noIsoSF", "_mu_trigUp", "_mu_trigDn", "_mu_MCPSFup", "_mu_MCPSFdn", //prompt muon systematics
  "_e_trigUp", "_e_trigDn", "_e_trkSFup", "_e_trkSFdn", "_e_IDSFup", "_e_IDSFdn", "_e_extraIDSFup", "_e_extraIDSFdn", //prompt electron systematics
  "_e_qflip_up", "_e_qflip_dn" //electron charge-flip rate 
};

