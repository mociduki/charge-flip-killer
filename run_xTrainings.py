#!/bin/usr/python

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import subprocess
import time
from datetime import datetime

rootsys= str(os.environ['ROOTSYS'])

doTraining=True

doXvalid=1
nfold=2
weightMethod = "compile"
bdtOption    = ""
nVarConfig   = 0x07e00

if len(sys.argv)>1: weightMethod = sys.argv[1]
if len(sys.argv)>2: bdtOption    = sys.argv[2]
if len(sys.argv)>3: pid_menu     = sys.argv[3]
if len(sys.argv)>4: nVarConfig   = int(sys.argv[4],16)
else : print "Compile only mode"

#prepare xTrainings directories if not yet created
print "Preparing directories and links if they don't exist..."

csi="" #comma separated indices

rootver= float(gROOT.GetVersion()[0:4])

#Training preparation
for i in range(nfold):
    dirpath= "train"+str(i)
    if not os.path.isdir(dirpath): os.mkdir(dirpath)

    if rootver>=6.08: dirpath+="/dataset"
    dirpath+="/weights"
    
    if not os.path.isdir(dirpath): os.makedirs(dirpath)
    if not os.path.islink("train"+str(i)+"/singleTraining.py"):    os.system("ln -s $PWD/singleTraining.py train"+str(i))
    if not os.path.islink("train"+str(i)+"/TMVAClassification.C"): os.system("ln -s $PWD/TMVAClassification.C train"+str(i))

    if i==0: csi += str(i)
    else:    csi += ","+str(i)
    pass

#Postprocess preparation
if not os.path.isdir("plots"):             os.mkdir("plots") #folder for TMVA automatic plots
if not os.path.isdir("plots/xValid_perf"): os.mkdir("plots/xValid_perf")
if not os.path.isdir("postprocess"):       os.mkdir("postprocess")

parentPath= os.environ["PWD"]
if   weightMethod=="none": weightMethod="" # "pe2s"
elif weightMethod=="compile":
    for i in range(nfold):
        workdir = parentPath+"/train"+str(i)+"/"
        os.chdir(workdir)
        print "Just to compile, knowing it will abort."
        os.system("python singleTraining.py");
        pass
    os.chdir(parentPath)
    gROOT.ProcessLine(".L eval.C+")
    gROOT.ProcessLine(".L utils/RejectionTool.C+")
    print ""
    print "\tCompilation finished!!"
    print ""
    sys.exit(0)
    pass

if not len(sys.argv)>3: sys.exit(1) # finish everything when compiling/preparing

#Run xTrainings in each directories
command_args = ["nohup","python","singleTraining.py","0/"+str(nfold),pid_menu,"0",weightMethod,bdtOption,str("0x{:05x}".format(nVarConfig))]

trainName = "pid"+command_args[4]+"_etaBin"+command_args[5]+"_"
if weightMethod!="": trainName+= weightMethod+"_"
trainName+= bdtOption + "_"
trainName+= str("0x{:05x}".format(nVarConfig))

print ""
print "\t # Submitting", nfold, "independent trainings and testings in subdirectories."
print ""

parallel_processing = 1

logPath="dataset/"
if not rootver>=6.08: logPath=""
logPath+="weights/TMVA_"+trainName+".log"

stopwatch=TStopwatch()
stopwatch.Start()

#main loop for submitting the subjobs
if doTraining:
    processes = list()    
    for i in range(nfold):
        if not(doXvalid) and i!=0: continue #KM: when no xValid, skip the other trainings than first one
        command_args[3] = str(i)+"/"+str(nfold)
        #if i==0: continue
        workdir = parentPath+"/train"+str(i)+"/"
        os.chdir(workdir)
        if os.path.isfile(logPath): os.remove(logPath)
        logObj0=open(logPath,"a")
        logObj0.flush()
        print "Submitting a job", command_args, "logged in", workdir+logPath
        # process0= subprocess.Popen(command_args,stdout=logObj0,stderr=logObj0)
        processes.append( subprocess.Popen(command_args,stdout=logObj0,stderr=logObj0) )
        
        if not parallel_processing:
            if processes[-1].wait() is None:
                print "\t Waiting a training to be finished, another check in another 5min."
                time.sleep(2*60)
                pass
            pass
        
        pass

    os.chdir(parentPath)            #################################################################
    time.sleep(1)
    
    if parallel_processing:
        all_finished = 0
        while not all_finished:
            print "\tTrainings still ongoing, wait for another 5min."
            time.sleep(5*60)
            
            tmp_flag = 1
            for process in processes: tmp_flag *= process.wait() is not None
            all_finished = tmp_flag
            pass
        pass
    
    all_finished_fine= True
    
    for process in processes: all_finished_fine *= not process.wait()
    
    #Check if both processes finished correctly
    if not all_finished_fine: 
        print ""
        print "One or all training processes failed, aborting!!!"
        print ""
        sys.exit()
        pass

    pass #if doTraining

stopwatch.Stop()

print ""
print "\t Printing elapsed time for the training, Real= ", stopwatch.RealTime()/60,"min, CPU(s)= ", stopwatch.CpuTime()
print "\t #", nfold, "independent trainings and testings over. Merging", nfold,"output files."
print ""

#hadd output files
hadd_cmd=""
if doXvalid:
    hadd_cmd= "hadd -f postprocess/TMVA_"+trainName+".root train{"+csi+"}/dataset/weights/TMVA_"+trainName+".root 2>/dev/null"
else:
    #hadd_cmd= "ln -s $PWD/train0/weights/TMVA_"+trainName+".root postprocess/TMVA_"+trainName+".root"
    hadd_cmd= "ln -s $PWD/train0/"
    if rootver>=6.08: hadd_cmd+="dataset/"
    hadd_cmd+="weights/TMVA_"+trainName+".root postprocess/TMVA_"+trainName+".root"
    pass
print hadd_cmd

os.system(hadd_cmd)

print ""
print "\t # Analyzing the merged output file."
print ""

#run evaluation
gROOT.ProcessLine(".L eval.C+")
gROOT.ProcessLine(".L utils/RejectionTool.C+")

skipProjection=0
evaluate(trainName,nfold,skipProjection)

os.system("rm -f train*/tmpfile_"+trainName)
