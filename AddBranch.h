//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue May  2 17:45:38 2017 by ROOT version 6.04/14
// from TTree Tree/Tree
// found on file: tree_Zee_ALL.root
//////////////////////////////////////////////////////////

#ifndef AddBranch_h
#define AddBranch_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class AddBranch {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   string m_trainName;

   // Declaration of leaf types
   Float_t         mee;
   Float_t         pt_ee;
   Float_t         eta_ee;
   Float_t         pt;
   Float_t         eta;
   Float_t         ptn_eta;
   Int_t           tagIsQualified;
   Int_t           tagD0Z0;
   Int_t           passD0Z0;
   Int_t           isFlip;
   Int_t           type;
   Int_t           event_number;
   Int_t           mc_number;
   Int_t           el_index;
   Int_t           hasTriggered;
   Int_t           isLoose;
   Int_t           isMedium;
   Int_t           isTight;
   Float_t         CFT_loose;
   Float_t         CFT_medium;
   Float_t         CFT_tight;
   Float_t         weight;
   Int_t           isOSpair;
   Int_t           pair_qual;
   Float_t         charge;
   Float_t         deltaphi1;
   Float_t         deltaphi2;
   Float_t         deltaphiLM;
   Float_t         deltaphiRes;
   Float_t         qoverpSig;
   Float_t         z0sintheta;
   Float_t         EoverP;
   Float_t         ptErr;
   Float_t         d0;
   Float_t         d0Err;
   Float_t         d0Sig;
   Float_t         cd0;
   Float_t         cd0Sig;
   Float_t         Rphi;
   Float_t         trt_llh;
   Float_t         chi2oftrackmatch;
   Float_t         chi2ndf;
   Float_t         deltaDeltaPhiFirstAndLM;
   Float_t         DeltaPOverP;
   Int_t           nSCTHits;
   Int_t           nBLHits;
   Float_t         dR;
   Float_t         avgCharge;
   Float_t           nTrk;
   Int_t           isolGradient;
   Int_t           isolGradientLoose;
   Int_t           isolFixedCutTight;
   Int_t           isolFixedCutLoose;

   Float_t         t_ECIDS_recon;
   Float_t         t_ECIDS_loose;
   Float_t         t_ECIDS_medium;
   Float_t         t_ECIDS_tight;
   Float_t         t_ECIDS_looseAndBL;

   Float_t         t_ECIDS_R21;

   // List of branches
   TBranch        *b_mee;   //!
   TBranch        *b_pt_ee;   //!
   TBranch        *b_eta_ee;   //!
   TBranch        *b_pt;   //!
   TBranch        *b_eta;   //!
   TBranch        *b_ptn_eta;   //!
   TBranch        *b_tagIsQualified;   //!
   TBranch        *b_tagD0Z0;   //!
   TBranch        *b_passD0Z0;   //!
   TBranch        *b_isFlip;   //!
   TBranch        *b_type;   //!
   TBranch        *b_event_number;   //!
   TBranch        *b_mc_number;   //!
   TBranch        *b_el_index;   //!
   TBranch        *b_hasTriggered;   //!
   TBranch        *b_isLoose;   //!
   TBranch        *b_isMedium;   //!
   TBranch        *b_isTight;   //!
   TBranch        *b_CFT_loose;   //!
   TBranch        *b_CFT_medium;   //!
   TBranch        *b_CFT_tight;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_isOSpair;   //!
   TBranch        *b_pair_qual;   //!
   TBranch        *b_charge;   //!
   TBranch        *b_deltaphi1;   //!
   TBranch        *b_deltaphi2;   //!
   TBranch        *b_deltaphiLM;   //!
   TBranch        *b_deltaphiRes;   //!
   TBranch        *b_qoverpSig;   //!
   TBranch        *b_z0sintheta;   //!
   TBranch        *b_EoverP;   //!
   TBranch        *b_ptErr;   //!
   TBranch        *b_d0;   //!
   TBranch        *b_d0Err;   //!
   TBranch        *b_d0Sig;   //!
   TBranch        *b_cd0;   //!
   TBranch        *b_cd0Sig;   //!
   TBranch        *b_Rphi;   //!
   TBranch        *b_trt_llh;   //!
   TBranch        *b_chi2oftrackmatch;   //!
   TBranch        *b_chi2ndf;   //!
   TBranch        *b_deltaDeltaPhiFirstAndLM;   //!
   TBranch        *b_DeltaPOverP;   //!
   TBranch        *b_nSCTHits;   //!
   TBranch        *b_nBLHits;   //!
   TBranch        *b_dR;   //!
   TBranch        *b_avgCharge;   //!
   TBranch        *b_nTrk;   //!
   TBranch        *b_isolGradient;   //!
   TBranch        *b_isolGradientLoose;   //!
   TBranch        *b_isolFixedCutTight;   //!
   TBranch        *b_isolFixedCutLoose;   //!

   AddBranch(TString inputfile, string trainName);
   virtual ~AddBranch();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef AddBranch_cxx
AddBranch::AddBranch(TString inputfile, string trainName) : fChain(0) 
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.

  m_trainName=trainName;
  
  TTree *tree = 0;
  
  const bool isData=inputfile.Contains("data");

  TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(inputfile);
  if (!f || !f->IsOpen())  f = new TFile(inputfile);
  f->GetObject("Tree",tree);
  
  Init(tree);
}

AddBranch::~AddBranch()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t AddBranch::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t AddBranch::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void AddBranch::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mee", &mee, &b_mee);
   fChain->SetBranchAddress("pt_ee", &pt_ee, &b_pt_ee);
   fChain->SetBranchAddress("eta_ee", &eta_ee, &b_eta_ee);
   fChain->SetBranchAddress("pt", &pt, &b_pt);
   fChain->SetBranchAddress("eta", &eta, &b_eta);
   fChain->SetBranchAddress("ptn_eta", &ptn_eta, &b_ptn_eta);
   fChain->SetBranchAddress("tagIsQualified", &tagIsQualified, &b_tagIsQualified);
   fChain->SetBranchAddress("tagD0Z0", &tagD0Z0, &b_tagD0Z0);
   fChain->SetBranchAddress("passD0Z0", &passD0Z0, &b_passD0Z0);
   fChain->SetBranchAddress("isFlip", &isFlip, &b_isFlip);
   fChain->SetBranchAddress("type", &type, &b_type);
   fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
   fChain->SetBranchAddress("mc_number", &mc_number, &b_mc_number);
   fChain->SetBranchAddress("el_index", &el_index, &b_el_index);
   fChain->SetBranchAddress("hasTriggered", &hasTriggered, &b_hasTriggered);
   fChain->SetBranchAddress("isLoose", &isLoose, &b_isLoose);
   fChain->SetBranchAddress("isMedium", &isMedium, &b_isMedium);
   fChain->SetBranchAddress("isTight", &isTight, &b_isTight);
   fChain->SetBranchAddress("CFT_loose", &CFT_loose, &b_CFT_loose);
   fChain->SetBranchAddress("CFT_medium", &CFT_medium, &b_CFT_medium);
   fChain->SetBranchAddress("CFT_tight", &CFT_tight, &b_CFT_tight);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("isOSpair", &isOSpair, &b_isOSpair);
   fChain->SetBranchAddress("pair_qual", &pair_qual, &b_pair_qual);
   fChain->SetBranchAddress("charge", &charge, &b_charge);
   fChain->SetBranchAddress("deltaphi1", &deltaphi1, &b_deltaphi1);
   fChain->SetBranchAddress("deltaphi2", &deltaphi2, &b_deltaphi2);
   fChain->SetBranchAddress("deltaphiLM", &deltaphiLM, &b_deltaphiLM);
   fChain->SetBranchAddress("deltaphiRes", &deltaphiRes, &b_deltaphiRes);
   fChain->SetBranchAddress("qoverpSig", &qoverpSig, &b_qoverpSig);
   fChain->SetBranchAddress("z0sintheta", &z0sintheta, &b_z0sintheta);
   fChain->SetBranchAddress("EoverP", &EoverP, &b_EoverP);
   fChain->SetBranchAddress("ptErr", &ptErr, &b_ptErr);
   fChain->SetBranchAddress("d0", &d0, &b_d0);
   fChain->SetBranchAddress("d0Err", &d0Err, &b_d0Err);
   fChain->SetBranchAddress("d0Sig", &d0Sig, &b_d0Sig);
   fChain->SetBranchAddress("cd0", &cd0, &b_cd0);
   fChain->SetBranchAddress("cd0Sig", &cd0Sig, &b_cd0Sig);
   fChain->SetBranchAddress("Rphi", &Rphi, &b_Rphi);
   fChain->SetBranchAddress("trt_llh", &trt_llh, &b_trt_llh);
   fChain->SetBranchAddress("chi2oftrackmatch", &chi2oftrackmatch, &b_chi2oftrackmatch);
   fChain->SetBranchAddress("chi2ndf", &chi2ndf, &b_chi2ndf);
   fChain->SetBranchAddress("deltaDeltaPhiFirstAndLM", &deltaDeltaPhiFirstAndLM, &b_deltaDeltaPhiFirstAndLM);
   fChain->SetBranchAddress("DeltaPOverP", &DeltaPOverP, &b_DeltaPOverP);
   fChain->SetBranchAddress("nSCTHits", &nSCTHits, &b_nSCTHits);
   fChain->SetBranchAddress("nBLHits", &nBLHits, &b_nBLHits);
   fChain->SetBranchAddress("dR", &dR, &b_dR);
   fChain->SetBranchAddress("avgCharge", &avgCharge, &b_avgCharge);
   fChain->SetBranchAddress("nTrk", &nTrk, &b_nTrk);
   fChain->SetBranchAddress("isolGradient", &isolGradient, &b_isolGradient);
   fChain->SetBranchAddress("isolGradientLoose", &isolGradientLoose, &b_isolGradientLoose);
   fChain->SetBranchAddress("isolFixedCutTight", &isolFixedCutTight, &b_isolFixedCutTight);
   fChain->SetBranchAddress("isolFixedCutLoose", &isolFixedCutLoose, &b_isolFixedCutLoose);
   Notify();
}

Bool_t AddBranch::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void AddBranch::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t AddBranch::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef AddBranch_cxx
