#!/bin/usr/python

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys= str(os.environ['ROOTSYS'])

if not os.path.isdir("trainingNtuple/"):
    os.mkdir("./trainingNtuple")
    pass

gROOT.ProcessLine(".L ntupleTransformer.C+")

#KM: only one wild card '*' is allowed in the statement below
#inputPath="/scratchdisk4/mociduki/egammaChargeFlipRejection/input/user.kristin.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.e3601_s2576_s2132_r7267_r6282.v4_SkimmedNtuple/*.root"
#inputPath="/scratchdisk4/mociduki/egammaChargeFlipRejection/input/Zee/mc_default/*.root*" # Zee for testing
#inputPath="/scratchdisk4/mociduki/egammaChargeFlipRejection/input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v4_SkimmedNtuple.root/output/*.root*" #single electron before fudging
#inputPath="/scratchdisk4/mociduki/egammaChargeFlipRejection/input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v5_SkimmedNtuple.root/output/*.root*" #single electron after  fudging
inputPath="input/data16_13TeV.periodAll/data16_13TeV*.root*"

PID=0

lumi = 20281.4;
if   PID == 0: PIDname = "ALL"
elif PID == 1: PIDname = "LOOSE"
elif PID == 2: PIDname = "MEDIUM"
elif PID == 3: PIDname = "TIGHT"

chflipAna= ntupleTransformer (inputPath, lumi, PIDname)
chflipAna.Loop();
print "EndOfRun"
