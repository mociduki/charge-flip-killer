#!/bin/usr/python

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys= str(os.environ['ROOTSYS'])

if not os.path.isdir("postprocess/"):
    os.mkdir("./postprocess")
    pass

gROOT.ProcessLine(".L eval.C+")
gROOT.ProcessLine(".L utils/RejectionTool.C+")

trainName="BDT_pid1_etaBin0_p2b_mlBDT_bG_t403_d5_c200_m0p5_0x1f010"
evaluate(trainName,0,0,0)
