#include <iostream>
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TString.h"
#include "TCut.h"
#include "TTree.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"

const TString var="type";
void type (
	   const TString sample = "Zee",
	   const int ptDo = 200,//GeV
	   const int ptUp = 300 //GeV
	   ) {
  gStyle->SetOptStat(0);

  if (not (sample=="Zee" or 
	   sample=="single"
	   ) ) {
    cout<<"Unrecognized sample!! Aborting.."<<endl;
    return;
  }

  TString file= "trainingNtuple/tree_alletas_ALL.root_"+sample ;
  std::cout<<"Opening file: "<<file<<std::endl;
  TFile* f = TFile::Open(file,"read");
  TTree* TestTree = (TTree*) f->Get("Tree");// "TestTree");

  unsigned nbins=50; float min=-2.5, max= 2.5;//for "eta"
  if      (var=="abs(eta)") min=0;  //else if (var=="type"        ) nbins=5,min=0,max=5;
  //else if (var=="pt"      ) nbins=50, min=-20e3, max=130e3;//for Zee
  else if (var=="type"      ) nbins=5, min=0, max=5;//for single el
  else {    cerr<<"UNRECOGNIZED VARIABLE, aborting!!"<<endl;exit(1);  }

  TH1F* h_sig_t2 = new TH1F("h_sig_t2","", nbins, min, max);
  TH1F* h_bkg_t2 = new TH1F("h_bkg_t2","", nbins, min, max);
  h_sig_t2->Sumw2();
  h_bkg_t2->Sumw2();

  TString commonCut = "isTight";

  TString ptCut = TString::Format("%d<pt/1e3 && pt/1e3<%d",ptDo,ptUp);

  commonCut= "( "+commonCut;
  commonCut+=" && "+ptCut + " )";

  TestTree->Project(h_sig_t2->GetName(), var,commonCut+" && isFlip==0", "norm");
  TestTree->Project(h_bkg_t2->GetName(), var,commonCut+" && isFlip==1", "norm");

  h_sig_t2->SetTitle(sample+" "+commonCut);
  //h_bkg_t2->SetTitle(sample+" "+commonCut);

  h_sig_t2->GetXaxis()->SetTitle(var);
  h_bkg_t2->GetXaxis()->SetTitle(var);

  h_bkg_t2->SetLineColor(kRed);  //h_bkg_t2->SetLineStyle(2);
  h_sig_t2->SetLineColor(kBlue); //h_sig_t2->SetLineStyle(2);
  h_bkg_t2->SetMarkerColor(kRed);  //h_bkg_t2->SetLineStyle(2);
  h_sig_t2->SetMarkerColor(kBlue); //h_sig_t2->SetLineStyle(2);

  h_bkg_t2->SetMarkerStyle(23);
  h_sig_t2->SetMarkerStyle(20);

  TLegend *leg = new TLegend(0.2,0.6,0.5,0.85);
  leg->AddEntry(h_sig_t2,"sig ","lep");
  leg->AddEntry(h_bkg_t2,"bkg ","lep");
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);

  h_sig_t2->SetMaximum(1);
  h_sig_t2->SetMinimum(0);
  h_sig_t2->Draw("e");
  h_bkg_t2->Draw("esame");

  leg->Draw();
  
  TCanvas *c1 = (TCanvas*)gROOT->FindObject("c1");
  c1->SaveAs("vars/"+var+"_"+sample+"_pt"+TString::Format("%d-%d",ptDo,ptUp)+".png");

}
