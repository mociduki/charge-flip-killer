//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan 12 14:28:55 2016 by ROOT version 5.34/25
// from TTree ZeeCandidate/ZeeCandidate
// found on file: /lcg/storage15/atlas/trepanier/Egamma/user.esauvan.6956615._000001.SkimmedNtuple.root
//////////////////////////////////////////////////////////

#ifndef ntupleTransformer_h
#define ntupleTransformer_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TLorentzVector.h"
#include "TBranch.h"
#include "utils/AtlasStyle.C"
#include "TMath.h"
#include "TRandom.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "utils/Utilities.h"

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <utility>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class ntupleTransformer {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   /* TTree* Tree_BkgMisId; */
   /* TTree* Tree_SignalGoodId; */
   /* TTree* Tree_sample0o2; */
   /* TTree* Tree_sample1o2; */
   vector<TTree*> Tree_samples;

   bool elCand1_isMisId = false, elCand1_isNotGood= false;
   bool elCand2_isMisId = false, elCand2_isNotGood= false;

   float t_mee;
   float t_pt_ee;
   float t_eta_ee;

   float t_pt;
   float t_eta;
   float t_partners_eta;

   int   t_tagIsQualified;
   int   t_tagD0Z0;
   int   t_passD0Z0;

   int   t_origin;
   int   t_type;
   int   t_newtype;
   int   t_isFlip;
   int   t_event_number;
   int   t_mc_number;
   int   t_el_index;
   int   t_pair_qual;
   int   t_hasTriggered;

   int   t_isLoose;
   int   t_isMedium;
   int   t_isTight;

   float t_CFT_loose;
   float t_CFT_medium;
   float t_CFT_tight;

   float t_CFT_pair_loose;
   float t_CFT_pair_medium;
   float t_CFT_pair_tight;

   float t_weight;
   int   t_isOSpair;
   float t_charge;
   float t_deltaphi1;
   float t_deltaphi2;
   float t_deltaphiLM;
   float t_deltaphiRes;
   float t_qoverpSig;
   float t_z0sintheta;
   float t_EoverP;
   float t_ptErr;
   float t_d0;
   float t_d0Err;
   float t_d0Sig;
   float t_ld0;
   float t_ld0Sig;
   float t_cd0;
   float t_cd0Sig;
   float t_Rphi;
   float t_trt_llh;

   int t_nTRTHits 	        ;
   int t_nTRTOutliers 	        ;
   int t_nTRTHighTHits 	        ;
   int t_nTRTHighTOutliers      ;
   int t_numberOfTRTDeadStraws  ;
   int t_numberOfTRTXenonHits   ;
   float t_TRTHighTOutliersRatio;

   float t_chi2oftrackmatch;
   //float t_ndftrackmatch;
   float t_chi2ndf;
   float t_deltaDeltaPhiFirstAndLM;
   float t_DeltaPOverP;
   float t_dR;
   float t_avgCharge;
   int   t_nTrk;
   int   t_nSCTHits;
   int   t_nBLHits;

   int   t_isolGradient;
   int   t_isolGradientLoose;
   int   t_isolFixedCutTight;
   int   t_isolFixedCutLoose;

   TString m_treename;
   Float_t m_weight;
   Float_t m_LUMI; 
   Bool_t m_mc11c; 
   Bool_t m_debug;
   TString m_PID;

   // Function in ntupleTransformer.C 
   vector<float> LOW();
   vector<float> UP();
   vector<int> NBINS();
   vector<string> HistoNames();
   vector<float> VarList_elec1();
   vector<float> VarList_elec2();
   float Theta(float);
   float Pz(float,float);
   void FillTreeVarElec(bool isLeadingEl);
   void FillTree(int evtNum);
   void setBranch(TTree* tree);
   //void FillTreeVarElec1();
   //void FillTreeVarElec2();

   Int_t isOSpair;
   Int_t pair_qual;
   bool isData=false;

   int elCand1_pid_qual=0,elCand2_pid_qual=0;

   // Declaration of leaf types
   Long64_t        EventNumber;
   Int_t           RunNumber;
   Int_t           RandomRunNumber;
   Int_t           MCChannelNumber;
   Int_t           RandomLumiBlockNumber;
   Int_t           Nvtx;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   Float_t         MCPileupWeight;
   Float_t         Zcand_M;
   Float_t         Zcand_pt;
   Float_t         Zcand_eta;
   Float_t         Zcand_phi;
   Float_t         Zcand_y;
   Int_t           isTagTag;
   Int_t           elCand1_isEMLoose;
   Int_t           elCand1_isEMMedium;
   Int_t           elCand1_isEMTight;
   Int_t           elCand1_isTightPP;
   Int_t           elCand1_isMediumPP;
   Int_t           elCand1_isLoosePP;
   Int_t           elCand1_isVeryLooseLL;
   Int_t           elCand1_isLooseLL;
   Int_t           elCand1_isMediumLL;
   Int_t           elCand1_isTightLL;
   Int_t           elCand1_isVeryTightLL;
   Float_t         elCand1_CFTloose;
   Float_t         elCand1_CFTmedium;
   Float_t         elCand1_CFTtight;
   Int_t           elCand1_isVeryLooseLL2015;
   Int_t           elCand1_isLooseLL2015;
   Int_t           elCand1_isMediumLL2015;
   Int_t           elCand1_isTightLL2015;
   Int_t           elCand1_isVeryLooseLL2015_CutD0DphiDeta;
   Int_t           elCand1_isLooseLL2015_CutD0DphiDeta;
   Int_t           elCand1_isMediumLL2015_CutD0DphiDeta;
   Int_t           elCand1_isTightLL2015_CutD0DphiDeta;
   Int_t           elCand1_isLoose2015;
   Int_t           elCand1_isMedium2015;
   Int_t           elCand1_isTight2015;
   Int_t           elCand1_isEMLoose2015;
   Int_t           elCand1_isEMMedium2015;
   Int_t           elCand1_isEMTight2015;
   Int_t           elCand1_isolTight;
   Int_t           elCand1_isolLoose;
   Int_t           elCand1_isolLooseTrackOnly;
   Int_t           elCand1_isolGradient;
   Int_t           elCand1_isolGradientLoose;
   Int_t           elCand1_isolFixedCutTightTrackOnly;
   Int_t           elCand1_FixedCutTight;
   Int_t           elCand1_FixedCutLoose;
   Int_t           elCand1_isTriggerMatched;
   Float_t         elCand1_cl_E;
   Float_t         elCand1_eta;
   Float_t         elCand1_cl_eta;
   Float_t         elCand1_etas2;
   Float_t         elCand1_phi;
   Float_t         elCand1_et;
   Float_t         elCand1_pt;
   Float_t         elCand1_charge;
   Float_t         elCand1_etcone20;
   Float_t         elCand1_etcone30;
   Float_t         elCand1_etcone40;
   Float_t         elCand1_ptcone20;
   Float_t         elCand1_ptcone30;
   Float_t         elCand1_ptcone40;
   Float_t         elCand1_ptvarcone20;
   Float_t         elCand1_ptvarcone30;
   Float_t         elCand1_ptvarcone40;
   Float_t         elCand1_topoetcone20;
   Float_t         elCand1_topoetcone30;
   Float_t         elCand1_topoetcone40;
   Float_t         elCand1_E233;
   Float_t         elCand1_E237;
   Float_t         elCand1_E277;
   Float_t         elCand1_Ethad;
   Float_t         elCand1_Ethad1;
   Float_t         elCand1_f1;
   Float_t         elCand1_f3;
   Float_t         elCand1_weta2;
   Float_t         elCand1_wstot;
   Float_t         elCand1_Emins1;
   Float_t         elCand1_emaxs1;
   Float_t         elCand1_Emax2;
   Float_t         elCand1_fracs1;
   Float_t         elCand1_reta;
   Float_t         elCand1_rhad0;
   Float_t         elCand1_rhad1;
   Float_t         elCand1_rhad;
   Float_t         elCand1_rphi;
   Float_t         elCand1_eratio;
   Float_t         elCand1_deltaeta1;
   Float_t         elCand1_deltaeta2;
   Float_t         elCand1_deltaphi1;
   Float_t         elCand1_deltaphi2;
   Float_t         elCand1_deltaphiRescaled;
   Float_t         elCand1_deltaphiFromLM;
   Float_t         elCand1_trackd0lifesign;
   Float_t         elCand1_trackd0pvunbiased;
   Float_t         elCand1_tracksigd0pvunbiased;
   Float_t         elCand1_d0significance;
   Float_t         elCand1_trackz0pvunbiased;
   Float_t         elCand1_EOverP;
   Float_t         elCand1_z0sinTheta;
   Float_t         elCand1_passd0z0;
   Float_t         elCand1_z0significance;
   Float_t         elCand1_qoverp;
   Float_t         elCand1_qoverpsignificance;
   Float_t         elCand1_chi2oftrackmatch;
   Float_t         elCand1_ndftrackmatch;
   Int_t           elCand1_numberOfInnermostPixelLayerHits;
   Int_t           elCand1_numberOfInnermostPixelLayerOutliers;
   Int_t           elCand1_expectInnermostPixelLayerHit;
   Int_t           elCand1_numberOfNextToInnermostPixelLayerHits;
   Int_t           elCand1_numberOfNextToInnermostPixelLayerOutliers;
   Int_t           elCand1_expectNextToInnermostPixelLayerHit;
   Int_t           elCand1_nBLHits;
   Int_t           elCand1_nBLayerOutliers;
   Int_t           elCand1_expectHitInBLayer;
   Int_t           elCand1_nPixHits;
   Int_t           elCand1_nPixelOutliers;
   Int_t           elCand1_nPixelDeadSensors;
   Int_t           elCand1_nSCTHits;
   Int_t           elCand1_nSCTOutliers;
   Int_t           elCand1_nSCTDeadSensors;
   Int_t           elCand1_nTRTHits;
   Int_t           elCand1_nTRTOutliers;
   Int_t           elCand1_nTRTHighTHits;
   Int_t           elCand1_nTRTHighTOutliers;
   Int_t           elCand1_numberOfTRTDeadStraws;
   Int_t           elCand1_numberOfTRTXenonHits;
   Float_t         elCand1_TRTHighTOutliersRatio;
   Float_t         elCand1_eProbabilityHT;
   Float_t         elCand1_DeltaPOverP;
   Float_t         elCand1_pTErr;
   Float_t         elCand1_deltaCurvOverErrCurv;
   Float_t         elCand1_deltaDeltaPhiFirstAndLM;
   Int_t           elCand1_type;
   Int_t           elCand1_origin;
   Int_t           elCand1_originbkg;
   Int_t           elCand1_typebkg;
   Int_t           elCand1_isTruthElectronFromZ;
   Int_t           elCand1_TruthParticlePdgId;
   Int_t           elCand1_firstEgMotherPdgId;
   Int_t           elCand1_TruthParticleBarcode;
   Int_t           elCand1_firstEgMotherBarcode;
   Int_t           elCand1_MotherPdgId;
   Int_t           elCand1_MotherBarcode;
   Int_t           elCand1_FirstEgMotherTyp;
   Int_t           elCand1_FirstEgMotherOrigin;
   Float_t         elCand1_dRPdgId;
   Float_t         elCand1_dR;
   Float_t         elCand1_averageCharge;
   Int_t           elCand1_nTrackParticles;

   Int_t           elCand2_isEMLoose;
   Int_t           elCand2_isEMMedium;
   Int_t           elCand2_isEMTight;
   Int_t           elCand2_isTightPP;
   Int_t           elCand2_isMediumPP;
   Int_t           elCand2_isLoosePP;
   Int_t           elCand2_isVeryLooseLL;
   Int_t           elCand2_isLooseLL;
   Int_t           elCand2_isMediumLL;
   Int_t           elCand2_isTightLL;
   Int_t           elCand2_isVeryTightLL;
   Float_t         elCand2_CFTloose;
   Float_t         elCand2_CFTmedium;
   Float_t         elCand2_CFTtight;
   Int_t           elCand2_isVeryLooseLL2015;
   Int_t           elCand2_isLooseLL2015;
   Int_t           elCand2_isMediumLL2015;
   Int_t           elCand2_isTightLL2015;
   Int_t           elCand2_isVeryLooseLL2015_CutD0DphiDeta;
   Int_t           elCand2_isLooseLL2015_CutD0DphiDeta;
   Int_t           elCand2_isMediumLL2015_CutD0DphiDeta;
   Int_t           elCand2_isTightLL2015_CutD0DphiDeta;
   Int_t           elCand2_isLoose2015;
   Int_t           elCand2_isMedium2015;
   Int_t           elCand2_isTight2015;
   Int_t           elCand2_isEMLoose2015;
   Int_t           elCand2_isEMMedium2015;
   Int_t           elCand2_isEMTight2015;
   Int_t           elCand2_isolTight;
   Int_t           elCand2_isolLoose;
   Int_t           elCand2_isolLooseTrackOnly;
   Int_t           elCand2_isolGradient;
   Int_t           elCand2_isolGradientLoose;
   Int_t           elCand2_isolFixedCutTightTrackOnly;
   Int_t           elCand2_FixedCutTight;
   Int_t           elCand2_FixedCutLoose;
   Int_t           elCand2_passDeltaE;  /* !!! */
   Int_t           elCand2_passFSide;  /* !!! */
   Int_t           elCand2_passWs3;  /* !!! */
   Int_t           elCand2_passEratio;  /* !!! */
   Int_t           elCand2_isTriggerMatched;
   Float_t         elCand2_cl_E;
   Float_t         elCand2_eta;
   Float_t         elCand2_cl_eta;
   Float_t         elCand2_etas2;
   Float_t         elCand2_phi;
   Float_t         elCand2_et;
   Float_t         elCand2_pt;
   Float_t         elCand2_charge;
   Float_t         elCand2_etcone20;
   Float_t         elCand2_etcone30;
   Float_t         elCand2_etcone40;
   Float_t         elCand2_ptcone20;
   Float_t         elCand2_ptcone30;
   Float_t         elCand2_ptcone40;
   Float_t         elCand2_ptvarcone20;
   Float_t         elCand2_ptvarcone30;
   Float_t         elCand2_ptvarcone40;
   Float_t         elCand2_topoetcone20;
   Float_t         elCand2_topoetcone30;
   Float_t         elCand2_topoetcone40;
   Float_t         elCand2_E233;
   Float_t         elCand2_E237;
   Float_t         elCand2_E277;
   Float_t         elCand2_Ethad;
   Float_t         elCand2_Ethad1;
   Float_t         elCand2_f1;
   Float_t         elCand2_f3;
   Float_t         elCand2_weta2;
   Float_t         elCand2_wstot;
   Float_t         elCand2_Emins1;
   Float_t         elCand2_emaxs1;
   Float_t         elCand2_Emax2;
   Float_t         elCand2_fracs1;
   Float_t         elCand2_reta;
   Float_t         elCand2_rhad0;
   Float_t         elCand2_rhad1;
   Float_t         elCand2_rhad;
   Float_t         elCand2_rphi;
   Float_t         elCand2_eratio;
   Float_t         elCand2_deltaeta1;
   Float_t         elCand2_deltaeta2;
   Float_t         elCand2_deltaphi1;
   Float_t         elCand2_deltaphi2;
   Float_t         elCand2_deltaphiRescaled;
   Float_t         elCand2_deltaphiFromLM;
   Float_t         elCand2_trackd0lifesign;
   Float_t         elCand2_trackd0pvunbiased;
   Float_t         elCand2_tracksigd0pvunbiased;
   Float_t         elCand2_d0significance;
   Float_t         elCand2_trackz0pvunbiased;
   Float_t         elCand2_EOverP;
   Float_t         elCand2_z0sinTheta;
   Float_t         elCand2_passd0z0;
   Float_t         elCand2_z0significance;
   Float_t         elCand2_qoverp;
   Float_t         elCand2_qoverpsignificance;
   Float_t         elCand2_chi2oftrackmatch;
   Float_t         elCand2_ndftrackmatch;
   Int_t           elCand2_numberOfInnermostPixelLayerHits;
   Int_t           elCand2_numberOfInnermostPixelLayerOutliers;
   Int_t           elCand2_expectInnermostPixelLayerHit;
   Int_t           elCand2_numberOfNextToInnermostPixelLayerHits;
   Int_t           elCand2_numberOfNextToInnermostPixelLayerOutliers;
   Int_t           elCand2_expectNextToInnermostPixelLayerHit;
   Int_t           elCand2_nBLHits;
   Int_t           elCand2_nBLayerOutliers;
   Int_t           elCand2_expectHitInBLayer;
   Int_t           elCand2_nPixHits;
   Int_t           elCand2_nPixelOutliers;
   Int_t           elCand2_nPixelDeadSensors;
   Int_t           elCand2_nSCTHits;
   Int_t           elCand2_nSCTOutliers;
   Int_t           elCand2_nSCTDeadSensors;
   Int_t           elCand2_nTRTHits;
   Int_t           elCand2_nTRTOutliers;
   Int_t           elCand2_nTRTHighTHits;
   Int_t           elCand2_nTRTHighTOutliers;
   Int_t           elCand2_numberOfTRTDeadStraws;
   Int_t           elCand2_numberOfTRTXenonHits;
   Float_t         elCand2_TRTHighTOutliersRatio;
   Float_t         elCand2_eProbabilityHT;
   Float_t         elCand2_DeltaPOverP;
   Float_t         elCand2_pTErr;
   Float_t         elCand2_deltaCurvOverErrCurv;
   Float_t         elCand2_deltaDeltaPhiFirstAndLM;
   Int_t           elCand2_type;
   Int_t           elCand2_origin;
   Int_t           elCand2_originbkg;
   Int_t           elCand2_typebkg;
   Int_t           elCand2_isTruthElectronFromZ;
   Int_t           elCand2_TruthParticlePdgId;
   Int_t           elCand2_firstEgMotherPdgId;
   Int_t           elCand2_TruthParticleBarcode;
   Int_t           elCand2_firstEgMotherBarcode;
   Int_t           elCand2_MotherPdgId;
   Int_t           elCand2_MotherBarcode;
   Int_t           elCand2_FirstEgMotherTyp;
   Int_t           elCand2_FirstEgMotherOrigin;
   Float_t         elCand2_dRPdgId;
   Float_t         elCand2_dR;
   Float_t         elCand2_averageCharge;
   Int_t           elCand2_nTrackParticles;

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_MCChannelNumber;   //!
   TBranch        *b_RandomLumiBlockNumber;   //!
   TBranch        *b_Nvtx;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_MCPileupWeight;   //!
   TBranch        *b_Zcand_M;   //!
   TBranch        *b_Zcand_pt;   //!
   TBranch        *b_Zcand_eta;   //!
   TBranch        *b_Zcand_phi;   //!
   TBranch        *b_Zcand_y;   //!
   TBranch        *b_isTagTag;   //!
   TBranch        *b_elCand1_isEMLoose;   //!
   TBranch        *b_elCand1_isEMMedium;   //!
   TBranch        *b_elCand1_isEMTight;   //!
   TBranch        *b_elCand1_isTightPP;   //!
   TBranch        *b_elCand1_isMediumPP;   //!
   TBranch        *b_elCand1_isLoosePP;   //!
   TBranch        *b_elCand1_isVeryLooseLL;   //!
   TBranch        *b_elCand1_isLooseLL;   //!
   TBranch        *b_elCand1_isMediumLL;   //!
   TBranch        *b_elCand1_isTightLL;   //!
   TBranch        *b_elCand1_isVeryTightLL;   //!
   TBranch        *b_elCand1_CFTloose;   //!
   TBranch        *b_elCand1_CFTmedium;   //!
   TBranch        *b_elCand1_CFTtight;   //!
   TBranch        *b_elCand1_isVeryLooseLL2015;   //!
   TBranch        *b_elCand1_isLooseLL2015;   //!
   TBranch        *b_elCand1_isMediumLL2015;   //!
   TBranch        *b_elCand1_isTightLL2015;   //!
   TBranch        *b_elCand1_isVeryLooseLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand1_isLooseLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand1_isMediumLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand1_isTightLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand1_isLoose2015;   //!
   TBranch        *b_elCand1_isMedium2015;   //!
   TBranch        *b_elCand1_isTight2015;   //!
   TBranch        *b_elCand1_isEMLoose2015;   //!
   TBranch        *b_elCand1_isEMMedium2015;   //!
   TBranch        *b_elCand1_isEMTight2015;   //!
   TBranch        *b_elCand1_isolTight;   //!
   TBranch        *b_elCand1_isolLoose;   //!
   TBranch        *b_elCand1_isolLooseTrackOnly;   //!
   TBranch        *b_elCand1_isolGradient;   //!
   TBranch        *b_elCand1_isolGradientLoose;   //!
   TBranch        *b_elCand1_isolFixedCutTightTrackOnly;   //!
   TBranch        *b_elCand1_FixedCutTight;   //!
   TBranch        *b_elCand1_FixedCutLoose;   //!
   TBranch        *b_elCand1_isTriggerMatched;   //!
   TBranch        *b_elCand1_cl_E;   //!
   TBranch        *b_elCand1_eta;   //!
   TBranch        *b_elCand1_cl_eta;   //!
   TBranch        *b_elCand1_etas2;   //!
   TBranch        *b_elCand1_phi;   //!
   TBranch        *b_elCand1_et;   //!
   TBranch        *b_elCand1_pt;   //!
   TBranch        *b_elCand1_charge;   //!
   TBranch        *b_elCand1_etcone20;   //!
   TBranch        *b_elCand1_etcone30;   //!
   TBranch        *b_elCand1_etcone40;   //!
   TBranch        *b_elCand1_ptcone20;   //!
   TBranch        *b_elCand1_ptcone30;   //!
   TBranch        *b_elCand1_ptcone40;   //!
   TBranch        *b_elCand1_ptvarcone20;   //!
   TBranch        *b_elCand1_ptvarcone30;   //!
   TBranch        *b_elCand1_ptvarcone40;   //!
   TBranch        *b_elCand1_topoetcone20;   //!
   TBranch        *b_elCand1_topoetcone30;   //!
   TBranch        *b_elCand1_topoetcone40;   //!
   TBranch        *b_elCand1_E233;   //!
   TBranch        *b_elCand1_E237;   //!
   TBranch        *b_elCand1_E277;   //!
   TBranch        *b_elCand1_Ethad;   //!
   TBranch        *b_elCand1_Ethad1;   //!
   TBranch        *b_elCand1_f1;   //!
   TBranch        *b_elCand1_f3;   //!
   TBranch        *b_elCand1_weta2;   //!
   TBranch        *b_elCand1_wstot;   //!
   TBranch        *b_elCand1_Emins1;   //!
   TBranch        *b_elCand1_emaxs1;   //!
   TBranch        *b_elCand1_Emax2;   //!
   TBranch        *b_elCand1_fracs1;   //!
   TBranch        *b_elCand1_reta;   //!
   TBranch        *b_elCand1_rhad0;   //!
   TBranch        *b_elCand1_rhad1;   //!
   TBranch        *b_elCand1_rhad;   //!
   TBranch        *b_elCand1_rphi;   //!
   TBranch        *b_elCand1_eratio;   //!
   TBranch        *b_elCand1_deltaeta1;   //!
   TBranch        *b_elCand1_deltaeta2;   //!
   TBranch        *b_elCand1_deltaphi1;   //!
   TBranch        *b_elCand1_deltaphi2;   //!
   TBranch        *b_elCand1_deltaphiRescaled;   //!
   TBranch        *b_elCand1_deltaphiFromLM;   //!
   TBranch        *b_elCand1_trackd0lifesign;   //!
   TBranch        *b_elCand1_trackd0pvunbiased;   //!
   TBranch        *b_elCand1_tracksigd0pvunbiased;   //!
   TBranch        *b_elCand1_d0significance;   //!
   TBranch        *b_elCand1_trackz0pvunbiased;   //!
   TBranch        *b_elCand1_EOverP;   //!
   TBranch        *b_elCand1_z0sinTheta;   //!
   TBranch        *b_elCand1_passd0z0;   //!
   TBranch        *b_elCand1_z0significance;   //!
   TBranch        *b_elCand1_qoverp;   //!
   TBranch        *b_elCand1_qoverpsignificance;   //!
   TBranch        *b_elCand1_chi2oftrackmatch;   //!
   TBranch        *b_elCand1_ndftrackmatch;   //!
   TBranch        *b_elCand1_numberOfInnermostPixelLayerHits;   //!
   TBranch        *b_elCand1_numberOfInnermostPixelLayerOutliers;   //!
   TBranch        *b_elCand1_expectInnermostPixelLayerHit;   //!
   TBranch        *b_elCand1_numberOfNextToInnermostPixelLayerHits;   //!
   TBranch        *b_elCand1_numberOfNextToInnermostPixelLayerOutliers;   //!
   TBranch        *b_elCand1_expectNextToInnermostPixelLayerHit;   //!
   TBranch        *b_elCand1_nBLHits;   //!
   TBranch        *b_elCand1_nBLayerOutliers;   //!
   TBranch        *b_elCand1_expectHitInBLayer;   //!
   TBranch        *b_elCand1_nPixHits;   //!
   TBranch        *b_elCand1_nPixelOutliers;   //!
   TBranch        *b_elCand1_nPixelDeadSensors;   //!
   TBranch        *b_elCand1_nSCTHits;   //!
   TBranch        *b_elCand1_nSCTOutliers;   //!
   TBranch        *b_elCand1_nSCTDeadSensors;   //!
   TBranch        *b_elCand1_nTRTHits;   //!
   TBranch        *b_elCand1_nTRTOutliers;   //!
   TBranch        *b_elCand1_nTRTHighTHits;   //!
   TBranch        *b_elCand1_nTRTHighTOutliers;   //!
   TBranch        *b_elCand1_numberOfTRTDeadStraws;   //!
   TBranch        *b_elCand1_numberOfTRTXenonHits;   //!
   TBranch        *b_elCand1_TRTHighTOutliersRatio;   //!
   TBranch        *b_elCand1_eProbabilityHT;   //!
   TBranch        *b_elCand1_DeltaPOverP;   //!
   TBranch        *b_elCand1_pTErr;   //!
   TBranch        *b_elCand1_deltaCurvOverErrCurv;   //!
   TBranch        *b_elCand1_deltaDeltaPhiFirstAndLM;   //!
   TBranch        *b_elCand1_type;   //!
   TBranch        *b_elCand1_origin;   //!
   TBranch        *b_elCand1_originbkg;   //!
   TBranch        *b_elCand1_typebkg;   //!
   TBranch        *b_elCand1_isTruthElectronFromZ;   //!
   TBranch        *b_elCand1_TruthParticlePdgId;   //!
   TBranch        *b_elCand1_firstEgMotherPdgId;   //!
   TBranch        *b_elCand1_TruthParticleBarcode;   //!
   TBranch        *b_elCand1_firstEgMotherBarcode;   //!
   TBranch        *b_elCand1_MotherPdgId;   //!
   TBranch        *b_elCand1_MotherBarcode;   //!
   TBranch        *b_elCand1_FirstEgMotherTyp;   //!
   TBranch        *b_elCand1_FirstEgMotherOrigin;   //!
   TBranch        *b_elCand1_dRPdgId;   //!
   TBranch        *b_elCand1_dR;   //!
   TBranch        *b_elCand1_averageCharge;   //!
   TBranch        *b_elCand1_nTrackParticles;   //!

   TBranch        *b_elCand2_isEMLoose;   //!
   TBranch        *b_elCand2_isEMMedium;   //!
   TBranch        *b_elCand2_isEMTight;   //!
   TBranch        *b_elCand2_isTightPP;   //!
   TBranch        *b_elCand2_isMediumPP;   //!
   TBranch        *b_elCand2_isLoosePP;   //!
   TBranch        *b_elCand2_isVeryLooseLL;   //!
   TBranch        *b_elCand2_isLooseLL;   //!
   TBranch        *b_elCand2_isMediumLL;   //!
   TBranch        *b_elCand2_isTightLL;   //!
   TBranch        *b_elCand2_isVeryTightLL;   //!
   TBranch        *b_elCand2_CFTloose;   //!
   TBranch        *b_elCand2_CFTmedium;   //!
   TBranch        *b_elCand2_CFTtight;   //!
   TBranch        *b_elCand2_isVeryLooseLL2015;   //!
   TBranch        *b_elCand2_isLooseLL2015;   //!
   TBranch        *b_elCand2_isMediumLL2015;   //!
   TBranch        *b_elCand2_isTightLL2015;   //!
   TBranch        *b_elCand2_isVeryLooseLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand2_isLooseLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand2_isMediumLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand2_isTightLL2015_CutD0DphiDeta;   //!
   TBranch        *b_elCand2_isLoose2015;   //!
   TBranch        *b_elCand2_isMedium2015;   //!
   TBranch        *b_elCand2_isTight2015;   //!
   TBranch        *b_elCand2_isEMLoose2015;   //!
   TBranch        *b_elCand2_isEMMedium2015;   //!
   TBranch        *b_elCand2_isEMTight2015;   //!
   TBranch        *b_elCand2_isolTight;   //!
   TBranch        *b_elCand2_isolLoose;   //!
   TBranch        *b_elCand2_isolLooseTrackOnly;   //!
   TBranch        *b_elCand2_isolGradient;   //!
   TBranch        *b_elCand2_isolGradientLoose;   //!
   TBranch        *b_elCand2_isolFixedCutTightTrackOnly;   //!
   TBranch        *b_elCand2_FixedCutTight;   //!
   TBranch        *b_elCand2_FixedCutLoose;   //!
   TBranch        *b_elCand2_passDeltaE;   //!
   TBranch        *b_elCand2_passFSide;   //!
   TBranch        *b_elCand2_passWs3;   //!
   TBranch        *b_elCand2_passEratio;   //!
   TBranch        *b_elCand2_isTriggerMatched;   //!
   TBranch        *b_elCand2_cl_E;   //!
   TBranch        *b_elCand2_eta;   //!
   TBranch        *b_elCand2_cl_eta;   //!
   TBranch        *b_elCand2_etas2;   //!
   TBranch        *b_elCand2_phi;   //!
   TBranch        *b_elCand2_et;   //!
   TBranch        *b_elCand2_pt;   //!
   TBranch        *b_elCand2_charge;   //!
   TBranch        *b_elCand2_etcone20;   //!
   TBranch        *b_elCand2_etcone30;   //!
   TBranch        *b_elCand2_etcone40;   //!
   TBranch        *b_elCand2_ptcone20;   //!
   TBranch        *b_elCand2_ptcone30;   //!
   TBranch        *b_elCand2_ptcone40;   //!
   TBranch        *b_elCand2_ptvarcone20;   //!
   TBranch        *b_elCand2_ptvarcone30;   //!
   TBranch        *b_elCand2_ptvarcone40;   //!
   TBranch        *b_elCand2_topoetcone20;   //!
   TBranch        *b_elCand2_topoetcone30;   //!
   TBranch        *b_elCand2_topoetcone40;   //!
   TBranch        *b_elCand2_E233;   //!
   TBranch        *b_elCand2_E237;   //!
   TBranch        *b_elCand2_E277;   //!
   TBranch        *b_elCand2_Ethad;   //!
   TBranch        *b_elCand2_Ethad1;   //!
   TBranch        *b_elCand2_f1;   //!
   TBranch        *b_elCand2_f3;   //!
   TBranch        *b_elCand2_weta2;   //!
   TBranch        *b_elCand2_wstot;   //!
   TBranch        *b_elCand2_Emins1;   //!
   TBranch        *b_elCand2_emaxs1;   //!
   TBranch        *b_elCand2_Emax2;   //!
   TBranch        *b_elCand2_fracs1;   //!
   TBranch        *b_elCand2_reta;   //!
   TBranch        *b_elCand2_rhad0;   //!
   TBranch        *b_elCand2_rhad1;   //!
   TBranch        *b_elCand2_rhad;   //!
   TBranch        *b_elCand2_rphi;   //!
   TBranch        *b_elCand2_eratio;   //!
   TBranch        *b_elCand2_deltaeta1;   //!
   TBranch        *b_elCand2_deltaeta2;   //!
   TBranch        *b_elCand2_deltaphi1;   //!
   TBranch        *b_elCand2_deltaphi2;   //!
   TBranch        *b_elCand2_deltaphiRescaled;   //!
   TBranch        *b_elCand2_deltaphiFromLM;   //!
   TBranch        *b_elCand2_trackd0lifesign;   //!
   TBranch        *b_elCand2_trackd0pvunbiased;   //!
   TBranch        *b_elCand2_tracksigd0pvunbiased;   //!
   TBranch        *b_elCand2_d0significance;   //!
   TBranch        *b_elCand2_trackz0pvunbiased;   //!
   TBranch        *b_elCand2_EOverP;   //!
   TBranch        *b_elCand2_z0sinTheta;   //!
   TBranch        *b_elCand2_passd0z0;   //!
   TBranch        *b_elCand2_z0significance;   //!
   TBranch        *b_elCand2_qoverp;   //!
   TBranch        *b_elCand2_qoverpsignificance;   //!
   TBranch        *b_elCand2_chi2oftrackmatch;   //!
   TBranch        *b_elCand2_ndftrackmatch;   //!
   TBranch        *b_elCand2_numberOfInnermostPixelLayerHits;   //!
   TBranch        *b_elCand2_numberOfInnermostPixelLayerOutliers;   //!
   TBranch        *b_elCand2_expectInnermostPixelLayerHit;   //!
   TBranch        *b_elCand2_numberOfNextToInnermostPixelLayerHits;   //!
   TBranch        *b_elCand2_numberOfNextToInnermostPixelLayerOutliers;   //!
   TBranch        *b_elCand2_expectNextToInnermostPixelLayerHit;   //!
   TBranch        *b_elCand2_nBLHits;   //!
   TBranch        *b_elCand2_nBLayerOutliers;   //!
   TBranch        *b_elCand2_expectHitInBLayer;   //!
   TBranch        *b_elCand2_nPixHits;   //!
   TBranch        *b_elCand2_nPixelOutliers;   //!
   TBranch        *b_elCand2_nPixelDeadSensors;   //!
   TBranch        *b_elCand2_nSCTHits;   //!
   TBranch        *b_elCand2_nSCTOutliers;   //!
   TBranch        *b_elCand2_nSCTDeadSensors;   //!
   TBranch        *b_elCand2_nTRTHits;   //!
   TBranch        *b_elCand2_nTRTOutliers;   //!
   TBranch        *b_elCand2_nTRTHighTHits;   //!
   TBranch        *b_elCand2_nTRTHighTOutliers;   //!
   TBranch        *b_elCand2_numberOfTRTDeadStraws;   //!
   TBranch        *b_elCand2_numberOfTRTXenonHits;   //!
   TBranch        *b_elCand2_TRTHighTOutliersRatio;   //!
   TBranch        *b_elCand2_eProbabilityHT;   //!
   TBranch        *b_elCand2_DeltaPOverP;   //!
   TBranch        *b_elCand2_pTErr;   //!
   TBranch        *b_elCand2_deltaCurvOverErrCurv;   //!
   TBranch        *b_elCand2_deltaDeltaPhiFirstAndLM;   //!
   TBranch        *b_elCand2_type;   //!
   TBranch        *b_elCand2_origin;   //!
   TBranch        *b_elCand2_originbkg;   //!
   TBranch        *b_elCand2_typebkg;   //!
   TBranch        *b_elCand2_isTruthElectronFromZ;   //!
   TBranch        *b_elCand2_TruthParticlePdgId;   //!
   TBranch        *b_elCand2_firstEgMotherPdgId;   //!
   TBranch        *b_elCand2_TruthParticleBarcode;   //!
   TBranch        *b_elCand2_firstEgMotherBarcode;   //!
   TBranch        *b_elCand2_MotherPdgId;   //!
   TBranch        *b_elCand2_MotherBarcode;   //!
   TBranch        *b_elCand2_FirstEgMotherTyp;   //!
   TBranch        *b_elCand2_FirstEgMotherOrigin;   //!
   TBranch        *b_elCand2_dRPdgId;   //!
   TBranch        *b_elCand2_dR;   //!
   TBranch        *b_elCand2_averageCharge;   //!
   TBranch        *b_elCand2_nTrackParticles;   //!

   ntupleTransformer(TString inputPath, Float_t LUMI, TString PIDname, TTree *tree=0);
   virtual ~ntupleTransformer();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();//const unsigned nfold=2;
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ntupleTransformer_cxx
ntupleTransformer::ntupleTransformer(TString inputPath, Float_t LUMI, TString PIDname, TTree *tree)
{
  /* m_inputPath = inputPath; */
  m_weight = 1.0;
  m_LUMI = LUMI;
  m_mc11c= false;
  m_PID = PIDname;

  if (tree == 0) {
    m_treename=inputPath.Contains("ParticleGun")?"eCandidate":"ZeeCandidate";
    if (not inputPath.Contains("ParticleGun")) isData = inputPath.Contains("period");
    

    TChain * chain = new TChain(m_treename);
    Float_t xsec = 1.0, kfac = 1.0, filter_eff = 1.0;
    Float_t  NeventDataset = 1.0;

    if (!m_treename.Contains("Zee")) MCChannelNumber=423000;
    xsec = 1901.2;
    kfac = 1.026;
    NeventDataset = 1;  // This is the sum of all EventWeight for all samples up here

    chain->Add(inputPath);

    tree = chain;
    cout<<"Added files to chain: "<<inputPath<<" with entries: "<<tree->GetEntries()<<endl;

    /* cout << "dataType = " << dataType << endl; */
    if(!inputPath.Contains("data")) {
      m_weight = LUMI*xsec*kfac*filter_eff/NeventDataset;
      m_weight = 1;
      cout << "LUMI = " << LUMI << ", xsec*kfac*filter_eff = " << xsec*kfac*filter_eff << ", tree nentries = " << tree->GetEntries() << ", NeventDataset = " << NeventDataset << endl;
      cout << "LUMI*xsec*kfac*filter_eff/NeventDataset = " << m_weight << endl;
    }
    else {
      cout<<endl;
      cout<<"you are currently running on data sample"<<endl;
      cout<<endl;
    }
   
  }
  Init(tree);
}

ntupleTransformer::~ntupleTransformer()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntupleTransformer::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntupleTransformer::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntupleTransformer::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   TString headname=m_treename.Contains("Zee")?"elCand1":"e";

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   if (m_treename.Contains("Zee")) {
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
   fChain->SetBranchAddress("MCChannelNumber",&MCChannelNumber, &b_MCChannelNumber);
   fChain->SetBranchAddress("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
   fChain->SetBranchAddress("Nvtx", &Nvtx, &b_Nvtx);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("MCPileupWeight", &MCPileupWeight, &b_MCPileupWeight);
   fChain->SetBranchAddress("Zcand_M", &Zcand_M, &b_Zcand_M);
   fChain->SetBranchAddress("Zcand_pt", &Zcand_pt, &b_Zcand_pt);
   fChain->SetBranchAddress("Zcand_eta", &Zcand_eta, &b_Zcand_eta);
   fChain->SetBranchAddress("Zcand_phi", &Zcand_phi, &b_Zcand_phi);
   fChain->SetBranchAddress("Zcand_y", &Zcand_y, &b_Zcand_y);
   fChain->SetBranchAddress("isTagTag", &isTagTag, &b_isTagTag);

   fChain->SetBranchAddress(headname+"_isEMLoose", &elCand1_isEMLoose, &b_elCand1_isEMLoose);
   fChain->SetBranchAddress(headname+"_isEMMedium", &elCand1_isEMMedium, &b_elCand1_isEMMedium);
   fChain->SetBranchAddress(headname+"_isEMTight", &elCand1_isEMTight, &b_elCand1_isEMTight);
   fChain->SetBranchAddress(headname+"_isTightPP", &elCand1_isTightPP, &b_elCand1_isTightPP);
   fChain->SetBranchAddress(headname+"_isMediumPP", &elCand1_isMediumPP, &b_elCand1_isMediumPP);
   fChain->SetBranchAddress(headname+"_isLoosePP", &elCand1_isLoosePP, &b_elCand1_isLoosePP);
   fChain->SetBranchAddress(headname+"_isVeryLooseLL", &elCand1_isVeryLooseLL, &b_elCand1_isVeryLooseLL);
   fChain->SetBranchAddress(headname+"_isLooseLL", &elCand1_isLooseLL, &b_elCand1_isLooseLL);
   fChain->SetBranchAddress(headname+"_isMediumLL", &elCand1_isMediumLL, &b_elCand1_isMediumLL);
   fChain->SetBranchAddress(headname+"_isTightLL", &elCand1_isTightLL, &b_elCand1_isTightLL);
   fChain->SetBranchAddress(headname+"_isVeryTightLL", &elCand1_isVeryTightLL, &b_elCand1_isVeryTightLL);

   fChain->SetBranchAddress(headname+"_CFTloose" , &elCand1_CFTloose , &b_elCand1_CFTloose);
   fChain->SetBranchAddress(headname+"_CFTmedium", &elCand1_CFTmedium, &b_elCand1_CFTmedium);
   fChain->SetBranchAddress(headname+"_CFTtight" , &elCand1_CFTtight , &b_elCand1_CFTtight);

   fChain->SetBranchAddress(headname+"_isVeryLooseLL2015", &elCand1_isVeryLooseLL2015, &b_elCand1_isVeryLooseLL2015);
   /* fChain->SetBranchAddress(headname+"_isLooseLL2015", &elCand1_isLooseLL2015, &b_elCand1_isLooseLL2015); */
   /* fChain->SetBranchAddress(headname+"_isMediumLL2015", &elCand1_isMediumLL2015, &b_elCand1_isMediumLL2015); */
   /* fChain->SetBranchAddress(headname+"_isTightLL2015", &elCand1_isTightLL2015, &b_elCand1_isTightLL2015); */
   fChain->SetBranchAddress(headname+"_isVeryLooseLL2015_CutD0DphiDeta", &elCand1_isVeryLooseLL2015_CutD0DphiDeta, &b_elCand1_isVeryLooseLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress(headname+"_isLooseLL2015_CutD0DphiDeta", &elCand1_isLooseLL2015_CutD0DphiDeta, &b_elCand1_isLooseLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress(headname+"_isMediumLL2015_CutD0DphiDeta", &elCand1_isMediumLL2015_CutD0DphiDeta, &b_elCand1_isMediumLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress(headname+"_isTightLL2015_CutD0DphiDeta", &elCand1_isTightLL2015_CutD0DphiDeta, &b_elCand1_isTightLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress(headname+"_isLoose2015", &elCand1_isLoose2015, &b_elCand1_isLoose2015);
   fChain->SetBranchAddress(headname+"_isMedium2015", &elCand1_isMedium2015, &b_elCand1_isMedium2015);
   fChain->SetBranchAddress(headname+"_isTight2015", &elCand1_isTight2015, &b_elCand1_isTight2015);
   fChain->SetBranchAddress(headname+"_isEMLoose2015", &elCand1_isEMLoose2015, &b_elCand1_isEMLoose2015);
   fChain->SetBranchAddress(headname+"_isEMMedium2015", &elCand1_isEMMedium2015, &b_elCand1_isEMMedium2015);
   fChain->SetBranchAddress(headname+"_isEMTight2015", &elCand1_isEMTight2015, &b_elCand1_isEMTight2015);
   fChain->SetBranchAddress(headname+"_isolTight", &elCand1_isolTight, &b_elCand1_isolTight);
   fChain->SetBranchAddress(headname+"_isolTight", &elCand1_isolTight, &b_elCand1_isolTight);
   fChain->SetBranchAddress(headname+"_isolLooseTrackOnly", &elCand1_isolLooseTrackOnly, &b_elCand1_isolLooseTrackOnly);
   fChain->SetBranchAddress(headname+"_isolGradient", &elCand1_isolGradient, &b_elCand1_isolGradient);
   fChain->SetBranchAddress(headname+"_isolGradientLoose", &elCand1_isolGradientLoose, &b_elCand1_isolGradientLoose);
   fChain->SetBranchAddress(headname+"_isolFixedCutTightTrackOnly", &elCand1_isolFixedCutTightTrackOnly, &b_elCand1_isolFixedCutTightTrackOnly);
   fChain->SetBranchAddress(headname+"_FixedCutTight", &elCand1_FixedCutTight, &b_elCand1_FixedCutTight);
   fChain->SetBranchAddress(headname+"_FixedCutLoose", &elCand1_FixedCutLoose, &b_elCand1_FixedCutLoose);
   }
   /* else { */
   fChain->SetBranchAddress(headname+"_isLooseLL2016_v11" , &elCand1_isLooseLL2015 , &b_elCand1_isLooseLL2015 );
   fChain->SetBranchAddress(headname+"_isTightLL2016_v11" , &elCand1_isTightLL2015 , &b_elCand1_isTightLL2015 );
   fChain->SetBranchAddress(headname+"_isMediumLL2016_v11", &elCand1_isMediumLL2015, &b_elCand1_isMediumLL2015);
   /* } */

   fChain->SetBranchAddress(headname+"_isTriggerMatched", &elCand1_isTriggerMatched, &b_elCand1_isTriggerMatched);

   fChain->SetBranchAddress(headname+"_cl_E", &elCand1_cl_E, &b_elCand1_cl_E);
   fChain->SetBranchAddress(headname+"_eta", &elCand1_eta, &b_elCand1_eta);
   if (m_treename.Contains("Zee") )
   fChain->SetBranchAddress(headname+"_cl_eta", &elCand1_cl_eta, &b_elCand1_cl_eta);
   fChain->SetBranchAddress(headname+"_etas2", &elCand1_etas2, &b_elCand1_etas2);
   fChain->SetBranchAddress(headname+"_phi", &elCand1_phi, &b_elCand1_phi);
   fChain->SetBranchAddress(headname+"_et", &elCand1_et, &b_elCand1_et);
   fChain->SetBranchAddress(headname+"_pt", &elCand1_pt, &b_elCand1_pt);
   fChain->SetBranchAddress(headname+"_charge", &elCand1_charge, &b_elCand1_charge);

   if (m_treename.Contains("Zee") ) {
   fChain->SetBranchAddress(headname+"_etcone20", &elCand1_etcone20, &b_elCand1_etcone20);
   fChain->SetBranchAddress(headname+"_etcone30", &elCand1_etcone30, &b_elCand1_etcone30);
   fChain->SetBranchAddress(headname+"_etcone40", &elCand1_etcone40, &b_elCand1_etcone40);
   fChain->SetBranchAddress(headname+"_ptcone20", &elCand1_ptcone20, &b_elCand1_ptcone20);
   fChain->SetBranchAddress(headname+"_ptcone30", &elCand1_ptcone30, &b_elCand1_ptcone30);
   fChain->SetBranchAddress(headname+"_ptcone40", &elCand1_ptcone40, &b_elCand1_ptcone40);
   fChain->SetBranchAddress(headname+"_ptvarcone20", &elCand1_ptvarcone20, &b_elCand1_ptvarcone20);
   fChain->SetBranchAddress(headname+"_ptvarcone30", &elCand1_ptvarcone30, &b_elCand1_ptvarcone30);
   fChain->SetBranchAddress(headname+"_ptvarcone40", &elCand1_ptvarcone40, &b_elCand1_ptvarcone40);
   fChain->SetBranchAddress(headname+"_topoetcone20", &elCand1_topoetcone20, &b_elCand1_topoetcone20);
   fChain->SetBranchAddress(headname+"_topoetcone30", &elCand1_topoetcone30, &b_elCand1_topoetcone30);
   fChain->SetBranchAddress(headname+"_topoetcone40", &elCand1_topoetcone40, &b_elCand1_topoetcone40);
   }
   else{
   fChain->SetBranchAddress(headname+"_Etcone20", &elCand1_etcone20, &b_elCand1_etcone20);
   fChain->SetBranchAddress(headname+"_Etcone30", &elCand1_etcone30, &b_elCand1_etcone30);
   fChain->SetBranchAddress(headname+"_Etcone40", &elCand1_etcone40, &b_elCand1_etcone40);
   fChain->SetBranchAddress(headname+"_ptcone20", &elCand1_ptcone20, &b_elCand1_ptcone20);
   }

   fChain->SetBranchAddress(headname+"_E233", &elCand1_E233, &b_elCand1_E233);
   fChain->SetBranchAddress(headname+"_E237", &elCand1_E237, &b_elCand1_E237);
   fChain->SetBranchAddress(headname+"_E277", &elCand1_E277, &b_elCand1_E277);
   fChain->SetBranchAddress(headname+"_Ethad", &elCand1_Ethad, &b_elCand1_Ethad);
   fChain->SetBranchAddress(headname+"_Ethad1", &elCand1_Ethad1, &b_elCand1_Ethad1);
   fChain->SetBranchAddress(headname+"_f1", &elCand1_f1, &b_elCand1_f1);
   fChain->SetBranchAddress(headname+"_f3", &elCand1_f3, &b_elCand1_f3);
   fChain->SetBranchAddress(headname+"_weta2", &elCand1_weta2, &b_elCand1_weta2);
   fChain->SetBranchAddress(headname+"_wstot", &elCand1_wstot, &b_elCand1_wstot);
   fChain->SetBranchAddress(headname+"_Emins1", &elCand1_Emins1, &b_elCand1_Emins1);
   fChain->SetBranchAddress(headname+"_emaxs1", &elCand1_emaxs1, &b_elCand1_emaxs1);
   fChain->SetBranchAddress(headname+"_Emax2", &elCand1_Emax2, &b_elCand1_Emax2);
   fChain->SetBranchAddress(headname+"_fracs1", &elCand1_fracs1, &b_elCand1_fracs1);
   fChain->SetBranchAddress(headname+"_reta", &elCand1_reta, &b_elCand1_reta);
   fChain->SetBranchAddress(headname+"_rhad0", &elCand1_rhad0, &b_elCand1_rhad0);
   fChain->SetBranchAddress(headname+"_rhad1", &elCand1_rhad1, &b_elCand1_rhad1);
   fChain->SetBranchAddress(headname+"_rhad", &elCand1_rhad, &b_elCand1_rhad);
   fChain->SetBranchAddress(headname+"_rphi", &elCand1_rphi, &b_elCand1_rphi);
   fChain->SetBranchAddress(headname+"_eratio", &elCand1_eratio, &b_elCand1_eratio);
   fChain->SetBranchAddress(headname+"_deltaeta1", &elCand1_deltaeta1, &b_elCand1_deltaeta1);

   if (m_treename.Contains("Zee") )
   fChain->SetBranchAddress(headname+"_deltaeta2", &elCand1_deltaeta2, &b_elCand1_deltaeta2);

   fChain->SetBranchAddress(headname+"_deltaphi1", &elCand1_deltaphi1, &b_elCand1_deltaphi1);
   fChain->SetBranchAddress(headname+"_deltaphi2", &elCand1_deltaphi2, &b_elCand1_deltaphi2);
   fChain->SetBranchAddress(headname+"_deltaphiRescaled", &elCand1_deltaphiRescaled, &b_elCand1_deltaphiRescaled);
   fChain->SetBranchAddress(headname+"_deltaphiFromLM", &elCand1_deltaphiFromLM, &b_elCand1_deltaphiFromLM);
   /* fChain->SetBranchAddress(headname+"_trackd0lifesign", &elCand1_trackd0lifesign, &b_elCand1_trackd0lifesign); */
   fChain->SetBranchAddress(headname+"_trackd0pvunbiased", &elCand1_trackd0pvunbiased, &b_elCand1_trackd0pvunbiased);
   fChain->SetBranchAddress(headname+"_tracksigd0pvunbiased", &elCand1_tracksigd0pvunbiased, &b_elCand1_tracksigd0pvunbiased);
   fChain->SetBranchAddress(headname+"_d0significance", &elCand1_d0significance, &b_elCand1_d0significance);

   if (m_treename.Contains("Zee") ) {
   fChain->SetBranchAddress(headname+"_trackz0pvunbiased", &elCand1_trackz0pvunbiased, &b_elCand1_trackz0pvunbiased);
   }

   fChain->SetBranchAddress(headname+"_EOverP", &elCand1_EOverP, &b_elCand1_EOverP);
   fChain->SetBranchAddress(headname+"_z0sinTheta", &elCand1_z0sinTheta, &b_elCand1_z0sinTheta);

   if (m_treename.Contains("Zee") ) {
   fChain->SetBranchAddress(headname+"_passd0z0", &elCand1_passd0z0, &b_elCand1_passd0z0);
   fChain->SetBranchAddress(headname+"_z0significance", &elCand1_z0significance, &b_elCand1_z0significance);
   fChain->SetBranchAddress(headname+"_qoverp", &elCand1_qoverp, &b_elCand1_qoverp);
   }
   fChain->SetBranchAddress(headname+"_qoverpsignificance", &elCand1_qoverpsignificance, &b_elCand1_qoverpsignificance);

   if (m_treename.Contains("Zee") )
   fChain->SetBranchAddress(headname+"_chi2oftrackmatch", &elCand1_chi2oftrackmatch, &b_elCand1_chi2oftrackmatch);
   else
   fChain->SetBranchAddress(headname+"_Chi2oftrackmatch", &elCand1_chi2oftrackmatch, &b_elCand1_chi2oftrackmatch);

   if (m_treename.Contains("Zee") ) {
   fChain->SetBranchAddress(headname+"_ndftrackmatch", &elCand1_ndftrackmatch, &b_elCand1_ndftrackmatch);
   fChain->SetBranchAddress(headname+"_numberOfInnermostPixelLayerHits", &elCand1_numberOfInnermostPixelLayerHits, &b_elCand1_numberOfInnermostPixelLayerHits);
   fChain->SetBranchAddress(headname+"_numberOfInnermostPixelLayerOutliers", &elCand1_numberOfInnermostPixelLayerOutliers, &b_elCand1_numberOfInnermostPixelLayerOutliers);
   fChain->SetBranchAddress(headname+"_expectInnermostPixelLayerHit", &elCand1_expectInnermostPixelLayerHit, &b_elCand1_expectInnermostPixelLayerHit);
   fChain->SetBranchAddress(headname+"_numberOfNextToInnermostPixelLayerHits", &elCand1_numberOfNextToInnermostPixelLayerHits, &b_elCand1_numberOfNextToInnermostPixelLayerHits);
   fChain->SetBranchAddress(headname+"_numberOfNextToInnermostPixelLayerOutliers", &elCand1_numberOfNextToInnermostPixelLayerOutliers, &b_elCand1_numberOfNextToInnermostPixelLayerOutliers);
   fChain->SetBranchAddress(headname+"_expectNextToInnermostPixelLayerHit", &elCand1_expectNextToInnermostPixelLayerHit, &b_elCand1_expectNextToInnermostPixelLayerHit);
   }

   fChain->SetBranchAddress(headname+"_nBLHits", &elCand1_nBLHits, &b_elCand1_nBLHits);
   fChain->SetBranchAddress(headname+"_nBLayerOutliers", &elCand1_nBLayerOutliers, &b_elCand1_nBLayerOutliers);
   fChain->SetBranchAddress(headname+"_expectHitInBLayer", &elCand1_expectHitInBLayer, &b_elCand1_expectHitInBLayer);
   fChain->SetBranchAddress(headname+"_nPixHits", &elCand1_nPixHits, &b_elCand1_nPixHits);
   fChain->SetBranchAddress(headname+"_nPixelOutliers", &elCand1_nPixelOutliers, &b_elCand1_nPixelOutliers);
   fChain->SetBranchAddress(headname+"_nPixelDeadSensors", &elCand1_nPixelDeadSensors, &b_elCand1_nPixelDeadSensors);
   fChain->SetBranchAddress(headname+"_nSCTHits", &elCand1_nSCTHits, &b_elCand1_nSCTHits);
   fChain->SetBranchAddress(headname+"_nSCTOutliers", &elCand1_nSCTOutliers, &b_elCand1_nSCTOutliers);
   fChain->SetBranchAddress(headname+"_nSCTDeadSensors", &elCand1_nSCTDeadSensors, &b_elCand1_nSCTDeadSensors);

   fChain->SetBranchAddress(headname+"_nTRTHits", &elCand1_nTRTHits, &b_elCand1_nTRTHits);
   fChain->SetBranchAddress(headname+"_nTRTOutliers", &elCand1_nTRTOutliers, &b_elCand1_nTRTOutliers);
   fChain->SetBranchAddress(headname+"_nTRTHighTHits", &elCand1_nTRTHighTHits, &b_elCand1_nTRTHighTHits);
   fChain->SetBranchAddress(headname+"_nTRTHighTOutliers", &elCand1_nTRTHighTOutliers, &b_elCand1_nTRTHighTOutliers);
   fChain->SetBranchAddress(headname+"_numberOfTRTDeadStraws", &elCand1_numberOfTRTDeadStraws, &b_elCand1_numberOfTRTDeadStraws);
   fChain->SetBranchAddress(headname+"_numberOfTRTXenonHits", &elCand1_numberOfTRTXenonHits, &b_elCand1_numberOfTRTXenonHits);
   fChain->SetBranchAddress(headname+"_TRTHighTOutliersRatio", &elCand1_TRTHighTOutliersRatio, &b_elCand1_TRTHighTOutliersRatio);
   fChain->SetBranchAddress(headname+"_eProbabilityHT", &elCand1_eProbabilityHT, &b_elCand1_eProbabilityHT);
   fChain->SetBranchAddress(headname+"_DeltaPOverP", &elCand1_DeltaPOverP, &b_elCand1_DeltaPOverP);
   fChain->SetBranchAddress(headname+"_pTErr", &elCand1_pTErr, &b_elCand1_pTErr);
   fChain->SetBranchAddress(headname+"_deltaCurvOverErrCurv", &elCand1_deltaCurvOverErrCurv, &b_elCand1_deltaCurvOverErrCurv);
   fChain->SetBranchAddress(headname+"_deltaDeltaPhiFirstAndLM", &elCand1_deltaDeltaPhiFirstAndLM, &b_elCand1_deltaDeltaPhiFirstAndLM);
   fChain->SetBranchAddress(headname+"_type", &elCand1_type, &b_elCand1_type);
   fChain->SetBranchAddress(headname+"_origin", &elCand1_origin, &b_elCand1_origin);
   fChain->SetBranchAddress(headname+"_originbkg", &elCand1_originbkg, &b_elCand1_originbkg);
   fChain->SetBranchAddress(headname+"_typebkg", &elCand1_typebkg, &b_elCand1_typebkg);

   if (m_treename.Contains("Zee") )
   fChain->SetBranchAddress(headname+"_isTruthElectronFromZ", &elCand1_isTruthElectronFromZ, &b_elCand1_isTruthElectronFromZ);

   fChain->SetBranchAddress(headname+"_firstEgMotherPdgId", &elCand1_firstEgMotherPdgId, &b_elCand1_firstEgMotherPdgId);
   fChain->SetBranchAddress(headname+"_TruthParticlePdgId", &elCand1_TruthParticlePdgId, &b_elCand1_TruthParticlePdgId);
   fChain->SetBranchAddress(headname+"_TruthParticleBarcode", &elCand1_TruthParticleBarcode, &b_elCand1_TruthParticleBarcode);
   fChain->SetBranchAddress(headname+"_firstEgMotherBarcode", &elCand1_firstEgMotherBarcode, &b_elCand1_firstEgMotherBarcode);
   fChain->SetBranchAddress(headname+"_MotherPdgId", &elCand1_MotherPdgId, &b_elCand1_MotherPdgId);
   fChain->SetBranchAddress(headname+"_MotherBarcode", &elCand1_MotherBarcode, &b_elCand1_MotherBarcode);
   fChain->SetBranchAddress(headname+"_FirstEgMotherTyp", &elCand1_FirstEgMotherTyp, &b_elCand1_FirstEgMotherTyp);
   fChain->SetBranchAddress(headname+"_FirstEgMotherOrigin", &elCand1_FirstEgMotherOrigin, &b_elCand1_FirstEgMotherOrigin);

   if (m_treename.Contains("Zee") ) {
   fChain->SetBranchAddress(headname+"_dRPdgId", &elCand1_dRPdgId, &b_elCand1_dRPdgId);
   fChain->SetBranchAddress(headname+"_dR", &elCand1_dR, &b_elCand1_dR);
   }
   fChain->SetBranchAddress(headname+"_averageCharge", &elCand1_averageCharge, &b_elCand1_averageCharge);
   fChain->SetBranchAddress(headname+"_nTrackParticles", &elCand1_nTrackParticles, &b_elCand1_nTrackParticles);

   if (m_treename.Contains("Zee")) {
   fChain->SetBranchAddress("elCand2_isEMLoose", &elCand2_isEMLoose, &b_elCand2_isEMLoose);
   fChain->SetBranchAddress("elCand2_isEMMedium", &elCand2_isEMMedium, &b_elCand2_isEMMedium);
   fChain->SetBranchAddress("elCand2_isEMTight", &elCand2_isEMTight, &b_elCand2_isEMTight);
   fChain->SetBranchAddress("elCand2_isTightPP", &elCand2_isTightPP, &b_elCand2_isTightPP);
   fChain->SetBranchAddress("elCand2_isMediumPP", &elCand2_isMediumPP, &b_elCand2_isMediumPP);
   fChain->SetBranchAddress("elCand2_isLoosePP", &elCand2_isLoosePP, &b_elCand2_isLoosePP);
   fChain->SetBranchAddress("elCand2_isVeryLooseLL", &elCand2_isVeryLooseLL, &b_elCand2_isVeryLooseLL);
   fChain->SetBranchAddress("elCand2_isLooseLL", &elCand2_isLooseLL, &b_elCand2_isLooseLL);
   fChain->SetBranchAddress("elCand2_isMediumLL", &elCand2_isMediumLL, &b_elCand2_isMediumLL);
   fChain->SetBranchAddress("elCand2_isTightLL", &elCand2_isTightLL, &b_elCand2_isTightLL);
   fChain->SetBranchAddress("elCand2_isVeryTightLL", &elCand2_isVeryTightLL, &b_elCand2_isVeryTightLL);

   fChain->SetBranchAddress("elCand2_CFTloose" , &elCand2_CFTloose , &b_elCand2_CFTloose);
   fChain->SetBranchAddress("elCand2_CFTmedium", &elCand2_CFTmedium, &b_elCand2_CFTmedium);
   fChain->SetBranchAddress("elCand2_CFTtight" , &elCand2_CFTtight , &b_elCand2_CFTtight);

   fChain->SetBranchAddress("elCand2_isVeryLooseLL2015", &elCand2_isVeryLooseLL2015, &b_elCand2_isVeryLooseLL2015);
   /* fChain->SetBranchAddress("elCand2_isLooseLL2015", &elCand2_isLooseLL2015, &b_elCand2_isLooseLL2015); */
   /* fChain->SetBranchAddress("elCand2_isMediumLL2015", &elCand2_isMediumLL2015, &b_elCand2_isMediumLL2015); */
   /* fChain->SetBranchAddress("elCand2_isTightLL2015", &elCand2_isTightLL2015, &b_elCand2_isTightLL2015); */
   fChain->SetBranchAddress("elCand2_isLooseLL2016_v11", &elCand2_isLooseLL2015, &b_elCand2_isLooseLL2015);
   fChain->SetBranchAddress("elCand2_isMediumLL2016_v11", &elCand2_isMediumLL2015, &b_elCand2_isMediumLL2015);
   fChain->SetBranchAddress("elCand2_isTightLL2016_v11", &elCand2_isTightLL2015, &b_elCand2_isTightLL2015);

   fChain->SetBranchAddress("elCand2_isVeryLooseLL2015_CutD0DphiDeta", &elCand2_isVeryLooseLL2015_CutD0DphiDeta, &b_elCand2_isVeryLooseLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress("elCand2_isLooseLL2015_CutD0DphiDeta", &elCand2_isLooseLL2015_CutD0DphiDeta, &b_elCand2_isLooseLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress("elCand2_isMediumLL2015_CutD0DphiDeta", &elCand2_isMediumLL2015_CutD0DphiDeta, &b_elCand2_isMediumLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress("elCand2_isTightLL2015_CutD0DphiDeta", &elCand2_isTightLL2015_CutD0DphiDeta, &b_elCand2_isTightLL2015_CutD0DphiDeta);
   fChain->SetBranchAddress("elCand2_isLoose2015", &elCand2_isLoose2015, &b_elCand2_isLoose2015);
   fChain->SetBranchAddress("elCand2_isMedium2015", &elCand2_isMedium2015, &b_elCand2_isMedium2015);
   fChain->SetBranchAddress("elCand2_isTight2015", &elCand2_isTight2015, &b_elCand2_isTight2015);
   fChain->SetBranchAddress("elCand2_isEMLoose2015", &elCand2_isEMLoose2015, &b_elCand2_isEMLoose2015);
   fChain->SetBranchAddress("elCand2_isEMMedium2015", &elCand2_isEMMedium2015, &b_elCand2_isEMMedium2015);
   fChain->SetBranchAddress("elCand2_isEMTight2015", &elCand2_isEMTight2015, &b_elCand2_isEMTight2015);
   fChain->SetBranchAddress("elCand2_isolTight", &elCand2_isolTight, &b_elCand2_isolTight);
   fChain->SetBranchAddress("elCand2_isolTight", &elCand2_isolTight, &b_elCand2_isolTight);
   fChain->SetBranchAddress("elCand2_isolLooseTrackOnly", &elCand2_isolLooseTrackOnly, &b_elCand2_isolLooseTrackOnly);
   fChain->SetBranchAddress("elCand2_isolGradient", &elCand2_isolGradient, &b_elCand2_isolGradient);
   fChain->SetBranchAddress("elCand2_isolGradientLoose", &elCand2_isolGradientLoose, &b_elCand2_isolGradientLoose);
   fChain->SetBranchAddress("elCand2_isolFixedCutTightTrackOnly", &elCand2_isolFixedCutTightTrackOnly, &b_elCand2_isolFixedCutTightTrackOnly);
   fChain->SetBranchAddress("elCand2_FixedCutTight", &elCand2_FixedCutTight, &b_elCand2_FixedCutTight);
   fChain->SetBranchAddress("elCand2_FixedCutLoose", &elCand2_FixedCutLoose, &b_elCand2_FixedCutLoose);
   fChain->SetBranchAddress("elCand2_isTriggerMatched", &elCand2_isTriggerMatched, &b_elCand2_isTriggerMatched);

   fChain->SetBranchAddress("elCand2_cl_E", &elCand2_cl_E, &b_elCand2_cl_E);
   fChain->SetBranchAddress("elCand2_eta", &elCand2_eta, &b_elCand2_eta);
   fChain->SetBranchAddress("elCand2_cl_eta", &elCand2_cl_eta, &b_elCand2_cl_eta);
   fChain->SetBranchAddress("elCand2_etas2", &elCand2_etas2, &b_elCand2_etas2);
   fChain->SetBranchAddress("elCand2_phi", &elCand2_phi, &b_elCand2_phi);
   fChain->SetBranchAddress("elCand2_et", &elCand2_et, &b_elCand2_et);
   fChain->SetBranchAddress("elCand2_pt", &elCand2_pt, &b_elCand2_pt);
   fChain->SetBranchAddress("elCand2_charge", &elCand2_charge, &b_elCand2_charge);
   fChain->SetBranchAddress("elCand2_etcone20", &elCand2_etcone20, &b_elCand2_etcone20);
   fChain->SetBranchAddress("elCand2_etcone30", &elCand2_etcone30, &b_elCand2_etcone30);
   fChain->SetBranchAddress("elCand2_etcone40", &elCand2_etcone40, &b_elCand2_etcone40);
   fChain->SetBranchAddress("elCand2_ptcone20", &elCand2_ptcone20, &b_elCand2_ptcone20);
   fChain->SetBranchAddress("elCand2_ptcone30", &elCand2_ptcone30, &b_elCand2_ptcone30);
   fChain->SetBranchAddress("elCand2_ptcone40", &elCand2_ptcone40, &b_elCand2_ptcone40);
   fChain->SetBranchAddress("elCand2_ptvarcone20", &elCand2_ptvarcone20, &b_elCand2_ptvarcone20);
   fChain->SetBranchAddress("elCand2_ptvarcone30", &elCand2_ptvarcone30, &b_elCand2_ptvarcone30);
   fChain->SetBranchAddress("elCand2_ptvarcone40", &elCand2_ptvarcone40, &b_elCand2_ptvarcone40);
   fChain->SetBranchAddress("elCand2_topoetcone20", &elCand2_topoetcone20, &b_elCand2_topoetcone20);
   fChain->SetBranchAddress("elCand2_topoetcone30", &elCand2_topoetcone30, &b_elCand2_topoetcone30);
   fChain->SetBranchAddress("elCand2_topoetcone40", &elCand2_topoetcone40, &b_elCand2_topoetcone40);
   fChain->SetBranchAddress("elCand2_E233", &elCand2_E233, &b_elCand2_E233);
   fChain->SetBranchAddress("elCand2_E237", &elCand2_E237, &b_elCand2_E237);
   fChain->SetBranchAddress("elCand2_E277", &elCand2_E277, &b_elCand2_E277);
   fChain->SetBranchAddress("elCand2_Ethad", &elCand2_Ethad, &b_elCand2_Ethad);
   fChain->SetBranchAddress("elCand2_Ethad1", &elCand2_Ethad1, &b_elCand2_Ethad1);
   fChain->SetBranchAddress("elCand2_f1", &elCand2_f1, &b_elCand2_f1);
   fChain->SetBranchAddress("elCand2_f3", &elCand2_f3, &b_elCand2_f3);
   fChain->SetBranchAddress("elCand2_weta2", &elCand2_weta2, &b_elCand2_weta2);
   fChain->SetBranchAddress("elCand2_wstot", &elCand2_wstot, &b_elCand2_wstot);
   fChain->SetBranchAddress("elCand2_Emins1", &elCand2_Emins1, &b_elCand2_Emins1);
   fChain->SetBranchAddress("elCand2_emaxs1", &elCand2_emaxs1, &b_elCand2_emaxs1);
   fChain->SetBranchAddress("elCand2_Emax2", &elCand2_Emax2, &b_elCand2_Emax2);
   fChain->SetBranchAddress("elCand2_fracs1", &elCand2_fracs1, &b_elCand2_fracs1);
   fChain->SetBranchAddress("elCand2_reta", &elCand2_reta, &b_elCand2_reta);
   fChain->SetBranchAddress("elCand2_rhad0", &elCand2_rhad0, &b_elCand2_rhad0);
   fChain->SetBranchAddress("elCand2_rhad1", &elCand2_rhad1, &b_elCand2_rhad1);
   fChain->SetBranchAddress("elCand2_rhad", &elCand2_rhad, &b_elCand2_rhad);
   fChain->SetBranchAddress("elCand2_rphi", &elCand2_rphi, &b_elCand2_rphi);
   fChain->SetBranchAddress("elCand2_eratio", &elCand2_eratio, &b_elCand2_eratio);
   fChain->SetBranchAddress("elCand2_deltaeta1", &elCand2_deltaeta1, &b_elCand2_deltaeta1);
   fChain->SetBranchAddress("elCand2_deltaeta2", &elCand2_deltaeta2, &b_elCand2_deltaeta2);
   fChain->SetBranchAddress("elCand2_deltaphi1", &elCand2_deltaphi1, &b_elCand2_deltaphi1);
   fChain->SetBranchAddress("elCand2_deltaphi2", &elCand2_deltaphi2, &b_elCand2_deltaphi2);
   fChain->SetBranchAddress("elCand2_deltaphiRescaled", &elCand2_deltaphiRescaled, &b_elCand2_deltaphiRescaled);
   fChain->SetBranchAddress("elCand2_deltaphiFromLM", &elCand2_deltaphiFromLM, &b_elCand2_deltaphiFromLM);
   /* fChain->SetBranchAddress("elCand2_trackd0lifesign", &elCand2_trackd0lifesign, &b_elCand2_trackd0lifesign); */
   fChain->SetBranchAddress("elCand2_trackd0pvunbiased", &elCand2_trackd0pvunbiased, &b_elCand2_trackd0pvunbiased);
   fChain->SetBranchAddress("elCand2_tracksigd0pvunbiased", &elCand2_tracksigd0pvunbiased, &b_elCand2_tracksigd0pvunbiased);
   fChain->SetBranchAddress("elCand2_d0significance", &elCand2_d0significance, &b_elCand2_d0significance);
   fChain->SetBranchAddress("elCand2_trackz0pvunbiased", &elCand2_trackz0pvunbiased, &b_elCand2_trackz0pvunbiased);
   fChain->SetBranchAddress("elCand2_EOverP", &elCand2_EOverP, &b_elCand2_EOverP);
   fChain->SetBranchAddress("elCand2_z0sinTheta", &elCand2_z0sinTheta, &b_elCand2_z0sinTheta);
   fChain->SetBranchAddress("elCand2_passd0z0", &elCand2_passd0z0, &b_elCand2_passd0z0);
   fChain->SetBranchAddress("elCand2_z0significance", &elCand2_z0significance, &b_elCand2_z0significance);
   fChain->SetBranchAddress("elCand2_qoverp", &elCand2_qoverp, &b_elCand2_qoverp);
   fChain->SetBranchAddress("elCand2_qoverpsignificance", &elCand2_qoverpsignificance, &b_elCand2_qoverpsignificance);
   fChain->SetBranchAddress("elCand2_chi2oftrackmatch", &elCand2_chi2oftrackmatch, &b_elCand2_chi2oftrackmatch);
   fChain->SetBranchAddress("elCand2_ndftrackmatch", &elCand2_ndftrackmatch, &b_elCand2_ndftrackmatch);
   fChain->SetBranchAddress("elCand2_numberOfInnermostPixelLayerHits", &elCand2_numberOfInnermostPixelLayerHits, &b_elCand2_numberOfInnermostPixelLayerHits);
   fChain->SetBranchAddress("elCand2_numberOfInnermostPixelLayerOutliers", &elCand2_numberOfInnermostPixelLayerOutliers, &b_elCand2_numberOfInnermostPixelLayerOutliers);
   fChain->SetBranchAddress("elCand2_expectInnermostPixelLayerHit", &elCand2_expectInnermostPixelLayerHit, &b_elCand2_expectInnermostPixelLayerHit);
   fChain->SetBranchAddress("elCand2_numberOfNextToInnermostPixelLayerHits", &elCand2_numberOfNextToInnermostPixelLayerHits, &b_elCand2_numberOfNextToInnermostPixelLayerHits);
   fChain->SetBranchAddress("elCand2_numberOfNextToInnermostPixelLayerOutliers", &elCand2_numberOfNextToInnermostPixelLayerOutliers, &b_elCand2_numberOfNextToInnermostPixelLayerOutliers);
   fChain->SetBranchAddress("elCand2_expectNextToInnermostPixelLayerHit", &elCand2_expectNextToInnermostPixelLayerHit, &b_elCand2_expectNextToInnermostPixelLayerHit);
   fChain->SetBranchAddress("elCand2_nBLHits", &elCand2_nBLHits, &b_elCand2_nBLHits);
   fChain->SetBranchAddress("elCand2_nBLayerOutliers", &elCand2_nBLayerOutliers, &b_elCand2_nBLayerOutliers);
   fChain->SetBranchAddress("elCand2_expectHitInBLayer", &elCand2_expectHitInBLayer, &b_elCand2_expectHitInBLayer);
   fChain->SetBranchAddress("elCand2_nPixHits", &elCand2_nPixHits, &b_elCand2_nPixHits);
   fChain->SetBranchAddress("elCand2_nPixelOutliers", &elCand2_nPixelOutliers, &b_elCand2_nPixelOutliers);
   fChain->SetBranchAddress("elCand2_nPixelDeadSensors", &elCand2_nPixelDeadSensors, &b_elCand2_nPixelDeadSensors);
   fChain->SetBranchAddress("elCand2_nSCTHits", &elCand2_nSCTHits, &b_elCand2_nSCTHits);
   fChain->SetBranchAddress("elCand2_nSCTOutliers", &elCand2_nSCTOutliers, &b_elCand2_nSCTOutliers);
   fChain->SetBranchAddress("elCand2_nSCTDeadSensors", &elCand2_nSCTDeadSensors, &b_elCand2_nSCTDeadSensors);
   fChain->SetBranchAddress("elCand2_nTRTHits", &elCand2_nTRTHits, &b_elCand2_nTRTHits);
   fChain->SetBranchAddress("elCand2_nTRTOutliers", &elCand2_nTRTOutliers, &b_elCand2_nTRTOutliers);
   fChain->SetBranchAddress("elCand2_nTRTHighTHits", &elCand2_nTRTHighTHits, &b_elCand2_nTRTHighTHits);
   fChain->SetBranchAddress("elCand2_nTRTHighTOutliers", &elCand2_nTRTHighTOutliers, &b_elCand2_nTRTHighTOutliers);
   fChain->SetBranchAddress("elCand2_numberOfTRTDeadStraws", &elCand2_numberOfTRTDeadStraws, &b_elCand2_numberOfTRTDeadStraws);
   fChain->SetBranchAddress("elCand2_numberOfTRTXenonHits", &elCand2_numberOfTRTXenonHits, &b_elCand2_numberOfTRTXenonHits);
   fChain->SetBranchAddress("elCand2_TRTHighTOutliersRatio", &elCand2_TRTHighTOutliersRatio, &b_elCand2_TRTHighTOutliersRatio);
   fChain->SetBranchAddress("elCand2_eProbabilityHT", &elCand2_eProbabilityHT, &b_elCand2_eProbabilityHT);
   fChain->SetBranchAddress("elCand2_DeltaPOverP", &elCand2_DeltaPOverP, &b_elCand2_DeltaPOverP);
   fChain->SetBranchAddress("elCand2_pTErr", &elCand2_pTErr, &b_elCand2_pTErr);
   fChain->SetBranchAddress("elCand2_deltaCurvOverErrCurv", &elCand2_deltaCurvOverErrCurv, &b_elCand2_deltaCurvOverErrCurv);
   fChain->SetBranchAddress("elCand2_deltaDeltaPhiFirstAndLM", &elCand2_deltaDeltaPhiFirstAndLM, &b_elCand2_deltaDeltaPhiFirstAndLM);
   fChain->SetBranchAddress("elCand2_type", &elCand2_type, &b_elCand2_type);
   fChain->SetBranchAddress("elCand2_origin", &elCand2_origin, &b_elCand2_origin);
   fChain->SetBranchAddress("elCand2_originbkg", &elCand2_originbkg, &b_elCand2_originbkg);
   fChain->SetBranchAddress("elCand2_typebkg", &elCand2_typebkg, &b_elCand2_typebkg);
   fChain->SetBranchAddress("elCand2_isTruthElectronFromZ", &elCand2_isTruthElectronFromZ, &b_elCand2_isTruthElectronFromZ);
   fChain->SetBranchAddress("elCand2_firstEgMotherPdgId", &elCand2_firstEgMotherPdgId, &b_elCand2_firstEgMotherPdgId);
   fChain->SetBranchAddress("elCand2_TruthParticlePdgId", &elCand2_TruthParticlePdgId, &b_elCand2_TruthParticlePdgId);
   fChain->SetBranchAddress("elCand2_TruthParticleBarcode", &elCand2_TruthParticleBarcode, &b_elCand2_TruthParticleBarcode);
   fChain->SetBranchAddress("elCand2_firstEgMotherBarcode", &elCand2_firstEgMotherBarcode, &b_elCand2_firstEgMotherBarcode);
   fChain->SetBranchAddress("elCand2_MotherPdgId", &elCand2_MotherPdgId, &b_elCand2_MotherPdgId);
   fChain->SetBranchAddress("elCand2_MotherBarcode", &elCand2_MotherBarcode, &b_elCand2_MotherBarcode);
   fChain->SetBranchAddress("elCand2_passDeltaE", &elCand2_passDeltaE, &b_elCand2_passDeltaE);
   fChain->SetBranchAddress("elCand2_passFSide", &elCand2_passFSide, &b_elCand2_passFSide);
   fChain->SetBranchAddress("elCand2_passWs3", &elCand2_passWs3, &b_elCand2_passWs3);
   fChain->SetBranchAddress("elCand2_passEratio", &elCand2_passEratio, &b_elCand2_passEratio);
   fChain->SetBranchAddress("elCand2_FirstEgMotherTyp", &elCand2_FirstEgMotherTyp, &b_elCand2_FirstEgMotherTyp);
   fChain->SetBranchAddress("elCand2_FirstEgMotherOrigin", &elCand2_FirstEgMotherOrigin, &b_elCand2_FirstEgMotherOrigin);
   fChain->SetBranchAddress("elCand2_dRPdgId", &elCand2_dRPdgId, &b_elCand2_dRPdgId);
   fChain->SetBranchAddress("elCand2_dR", &elCand2_dR, &b_elCand2_dR);
   fChain->SetBranchAddress("elCand2_averageCharge", &elCand2_averageCharge, &b_elCand2_averageCharge);
   fChain->SetBranchAddress("elCand2_nTrackParticles", &elCand2_nTrackParticles, &b_elCand2_nTrackParticles);
   }
   Notify();
}

Bool_t ntupleTransformer::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntupleTransformer::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntupleTransformer::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntupleTransformer_cxx
