#!/bin/usr/python

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys= str(os.environ['ROOTSYS'])

gROOT.ProcessLine(".L TMVAClassification.C+")


if len(sys.argv)<6:
    print "Missing arguments. Aborting.."
    print ""
    print "USAGE: # python singleTraining.py 0/2     3   0      \'\'      t100_d5_c200_m2 0x07e00"
    print "       # python singleTraining.py folding pid etabin weighting bdtOption       nVarConfig"
    print ""
    sys.exit(1)
    pass

trainSample=sys.argv[1]
pid        =int(sys.argv[2])
etaBin     =int(sys.argv[3])
wMethod    =str(sys.argv[4])
bdtOption  =str(sys.argv[5])
nVarConfig =int(sys.argv[6],16) #originally treated as str

#inputPath=os.environ['CFK_TrainingDir'] + "/"
#inputPathTr="../trainingNtuple/tree_single_ALL.root"
#inputPathTe="../trainingNtuple/tree_Zee_ALL.root"
inputPathTr="../trainingNtuple/tree_data_ALL.root"
inputPathTe="" # "../trainingNtuple/tree_data_ALL.root"

print "Loading input file: ", inputPathTr

if not trainSample.find("/"):
    print "trainSample", trainSample, "does not contain \"/\". It should be formatted like \"0/2\". Aborting.."
    sys.exit(1)
    pass

if pid>3:
    print "pid>3!! Aborting.."
    sys.exit(1)
    pass

if etaBin>3:
    print "etaBin>3!! Aborting.."
    sys.exit(1)
    pass

TMVAClassification(trainSample,pid,etaBin,wMethod,bdtOption,inputPathTr,inputPathTe,nVarConfig)
if wMethod!="": wMethod+="_"
trainingName="pid"+str(pid)+"_etaBin"+str(etaBin)+"_"+wMethod+bdtOption+"_"+str(hex(nVarConfig))
filename= "tmpfile_"+trainingName
if os.path.isfile(filename): os.remove(filename)
