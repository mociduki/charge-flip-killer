//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Oct 24 15:42:18 2016 by ROOT version 6.04/14
// from TTree tree/tree
// found on file: ../input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v1_SkimmedNtuple.root/user.kristin.9692952._000001.SkimmedNtuple.root
//////////////////////////////////////////////////////////

#ifndef vec_removal_h
#define vec_removal_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class vec_removal {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         Weight;
   UInt_t          EventNumber;
   Float_t         Nvtx;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   vector<float>   *el_cl_E;
   vector<float>   *el_etas2;
   vector<float>   *el_phi;
   vector<float>   *el_et;
   vector<float>   *el_pt;
   vector<float>   *el_charge;
   vector<float>   *el_Etcone20;
   vector<float>   *el_Etcone30;
   vector<float>   *el_Etcone40;
   vector<float>   *el_ptcone20;
   vector<float>   *el_E233;
   vector<float>   *el_E237;
   vector<float>   *el_E277;
   vector<float>   *el_Ethad;
   vector<float>   *el_Ethad1;
   vector<float>   *el_f1;
   vector<float>   *el_f3;
   vector<float>   *el_weta2;
   vector<float>   *el_wstot;
   vector<float>   *el_Emins1;
   vector<float>   *el_emaxs1;
   vector<float>   *el_Emax2;
   vector<float>   *el_fracs1;
   vector<float>   *el_reta;
   vector<float>   *el_rhad0;
   vector<float>   *el_rhad1;
   vector<float>   *el_rhad;
   vector<float>   *el_rphi;
   vector<float>   *el_eratio;
   vector<float>   *el_deltaeta1;
   vector<float>   *el_deltaphi2;
   vector<float>   *el_deltaphi1;
   vector<float>   *el_deltaphiRescaled;
   vector<float>   *el_deltaphiFromLM;
   vector<float>   *el_trackd0pvunbiased;
   vector<float>   *el_tracksigd0pvunbiased;
   vector<float>   *el_d0significance;
   vector<float>   *el_Chi2oftrackmatch;
   /* vector<float>   *el_charge; */
   vector<float>   *el_z0sinTheta;
   vector<float>   *el_z0;
   vector<float>   *el_EOverP;

   vector<float>   *el_qoverpsignificance;
   vector<int>     *el_isolTight;
   vector<int>     *el_isolLoose;
   vector<int>     *el_isolLooseTrackOnly;
   vector<int>     *el_isolGradient;
   vector<int>     *el_isolGradientLoose;
   vector<int>     *el_isolFixedCutTightTrackOnly;
   vector<int>     *el_FixedCutTight;
   vector<int>     *el_FixedCutLoose;
   vector<int>     *el_isVeryLooseLL2016_v11;
   vector<int>     *el_isLooseLL2016_v11;
   vector<int>     *el_isLooseAndBLayerLL2016_v11;
   vector<int>     *el_isMediumLL2016_v11;
   vector<int>     *el_isTightLL2016_v11;
   vector<int>     *el_TightLLH_80GeVHighETFixThreshold_v11;
   vector<int>     *el_isVeryLooseLLH_Smooth_v11;
   vector<int>     *el_isLooseLLH_Smooth_v11;
   vector<int>     *el_isLooseAndBLayerLLH_Smooth_v11;
   vector<int>     *el_isMediumLLH_Smooth_v11;
   vector<int>     *el_isTightLLH_Smooth_v11;
   vector<int>     *el_TightLLH_80GeVHighETFixThreshold_Smooth_v11;
   vector<float>   *el_CFT;
   vector<int>     *el_passCFT;

   vector<int>     *el_nBLHits;
   vector<int>     *el_nBLayerOutliers;
   vector<int>     *el_expectHitInBLayer;
   vector<int>     *el_nPixHits;
   vector<int>     *el_nPixelOutliers;
   vector<int>     *el_nPixelDeadSensors;
   vector<int>     *el_nSCTHits;
   vector<int>     *el_nSCTOutliers;
   vector<int>     *el_nSCTDeadSensors;
   vector<int>     *el_nTRTHits;
   vector<int>     *el_nTRTOutliers;
   vector<int>     *el_nTRTHighTHits;
   vector<int>     *el_nTRTHighTOutliers;
   vector<int>     *el_numberOfTRTDeadStraws;
   vector<int>     *el_numberOfTRTXenonHits;
   vector<float>   *el_TRTHighTOutliersRatio;
   vector<float>   *el_eProbabilityHT;
   vector<float>   *el_DeltaPOverP;
   vector<float>   *el_pTErr;
   vector<float>   *el_deltaCurvOverErrCurv;
   vector<float>   *el_deltaDeltaPhiFirstAndLM;
   vector<int>     *el_type;
   vector<int>     *el_origin;
   vector<int>     *el_originbkg;
   vector<int>     *el_isTruthElectronFromZ;
   /* vector<int>     *el_originbkg; */
   vector<int>     *el_typebkg;
   vector<int>     *el_TruthParticlePdgId;
   vector<int>     *el_firstEgMotherPdgId;
   vector<int>     *el_TruthParticleBarcode;
   vector<int>     *el_firstEgMotherBarcode;
   vector<int>     *el_MotherPdgId;
   vector<int>     *el_MotherBarcode;
   vector<int>     *el_FirstEgMotherTyp;
   vector<int>     *el_FirstEgMotherOrigin;
   vector<float>   *trig_EF_el_cl_E;
   vector<float>   *trig_EF_el_etas2;
   vector<float>   *trig_EF_el_phi;
   vector<float>   *trig_EF_el_et;
   vector<float>   *trig_EF_el_pt;
   vector<float>   *trig_EF_el_charge;
   vector<float>   *trig_EF_el_Etcone20;
   vector<float>   *trig_EF_el_Etcone30;
   vector<float>   *trig_EF_el_Etcone40;
   vector<float>   *trig_EF_el_ptcone20;
   vector<float>   *trig_EF_el_E233;
   vector<float>   *trig_EF_el_E237;
   vector<float>   *trig_EF_el_E277;
   vector<float>   *trig_EF_el_Ethad;
   vector<float>   *trig_EF_el_Ethad1;
   vector<float>   *trig_EF_el_f1;
   vector<float>   *trig_EF_el_f3;
   vector<float>   *trig_EF_el_weta2;
   vector<float>   *trig_EF_el_wstot;
   vector<float>   *trig_EF_el_Emins1;
   vector<float>   *trig_EF_el_emaxs1;
   vector<float>   *trig_EF_el_Emax2;
   vector<float>   *trig_EF_el_fracs1;
   vector<float>   *trig_EF_el_reta;
   vector<float>   *trig_EF_el_rhad0;
   vector<float>   *trig_EF_el_rhad1;
   vector<float>   *trig_EF_el_rhad;
   vector<float>   *trig_EF_el_rphi;
   vector<float>   *trig_EF_el_eratio;
   vector<float>   *trig_EF_el_deltaeta1;
   vector<float>   *trig_EF_el_deltaphi2;
   vector<float>   *trig_EF_el_deltaphiRescaled;
   vector<float>   *trig_EF_el_deltaphiFromLM;
   vector<float>   *trig_EF_el_trackd0pvunbiased;
   vector<float>   *trig_EF_el_tracksigd0pvunbiased;
   vector<float>   *trig_EF_el_d0significance;
   vector<float>   *trig_EF_el_z0;
   vector<float>   *trig_EF_el_EOverP;
   vector<int>     *trig_EF_el_nBLHits;
   vector<int>     *trig_EF_el_nBLayerOutliers;
   vector<int>     *trig_EF_el_expectHitInBLayer;
   vector<int>     *trig_EF_el_nPixHits;
   vector<int>     *trig_EF_el_nPixelOutliers;
   vector<int>     *trig_EF_el_nPixelDeadSensors;
   vector<int>     *trig_EF_el_nSCTHits;
   vector<int>     *trig_EF_el_nSCTOutliers;
   vector<int>     *trig_EF_el_nSCTDeadSensors;
   vector<int>     *trig_EF_el_nTRTHits;
   vector<int>     *trig_EF_el_nTRTOutliers;
   vector<int>     *trig_EF_el_nTRTHighTHits;
   vector<int>     *trig_EF_el_nTRTHighTOutliers;
   vector<int>     *trig_EF_el_numberOfTRTDeadStraws;
   vector<int>     *trig_EF_el_numberOfTRTXenonHits;
   vector<float>   *trig_EF_el_TRTHighTOutliersRatio;
   vector<float>   *trig_EF_el_eProbabilityHT;
   vector<float>   *trig_EF_el_DeltaPOverP;
   vector<float>   *trig_EF_el_pTErr;
   vector<float>   *trig_EF_el_deltaCurvOverErrCurv;
   vector<float>   *trig_EF_el_deltaDeltaPhiFirstAndLM;

   // List of branches
   TBranch        *b_Weight;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_Nvtx;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_el_cl_E;   //!
   TBranch        *b_el_etas2;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_et;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_Etcone20;   //!
   TBranch        *b_el_Etcone30;   //!
   TBranch        *b_el_Etcone40;   //!
   TBranch        *b_el_ptcone20;   //!
   TBranch        *b_el_E233;   //!
   TBranch        *b_el_E237;   //!
   TBranch        *b_el_E277;   //!
   TBranch        *b_el_Ethad;   //!
   TBranch        *b_el_Ethad1;   //!
   TBranch        *b_el_f1;   //!
   TBranch        *b_el_f3;   //!
   TBranch        *b_el_weta2;   //!
   TBranch        *b_el_wstot;   //!
   TBranch        *b_el_Emins1;   //!
   TBranch        *b_el_emaxs1;   //!
   TBranch        *b_el_Emax2;   //!
   TBranch        *b_el_fracs1;   //!
   TBranch        *b_el_reta;   //!
   TBranch        *b_el_rhad0;   //!
   TBranch        *b_el_rhad1;   //!
   TBranch        *b_el_rhad;   //!
   TBranch        *b_el_rphi;   //!
   TBranch        *b_el_eratio;   //!
   TBranch        *b_el_deltaeta1;   //!
   TBranch        *b_el_deltaphi2;   //!
   TBranch        *b_el_deltaphi1;   //!
   TBranch        *b_el_deltaphiRescaled;   //!
   TBranch        *b_el_deltaphiFromLM;   //!
   TBranch        *b_el_trackd0pvunbiased;   //!
   TBranch        *b_el_tracksigd0pvunbiased;   //!
   TBranch        *b_el_d0significance;   //!
   TBranch        *b_el_Chi2oftrackmatch;   //!
   /* TBranch        *b_el_charge;   //! */
   TBranch        *b_el_z0sinTheta;   //!
   TBranch        *b_el_z0;   //!
   TBranch        *b_el_EOverP;   //!

   TBranch        *b_el_qoverpsignificance;   //!
   TBranch        *b_el_isolTight;   //!
   TBranch        *b_el_isolLoose;   //!
   TBranch        *b_el_isolLooseTrackOnly;   //!
   TBranch        *b_el_isolGradient;   //!
   TBranch        *b_el_isolGradientLoose;   //!
   TBranch        *b_el_isolFixedCutTightTrackOnly;   //!
   TBranch        *b_el_FixedCutTight;   //!
   TBranch        *b_el_FixedCutLoose;   //!
   TBranch        *b_el_isVeryLooseLL2016_v11;   //!
   TBranch        *b_el_isLooseLL2016_v11;   //!
   TBranch        *b_el_isLooseAndBLayerLL2016_v11;   //!
   TBranch        *b_el_isMediumLL2016_v11;   //!
   TBranch        *b_el_isTightLL2016_v11;   //!
   TBranch        *b_el_TightLLH_80GeVHighETFixThreshold_v11;
   TBranch        *b_el_isVeryLooseLLH_Smooth_v11;   //!
   TBranch        *b_el_isLooseLLH_Smooth_v11;   //!
   TBranch        *b_el_isLooseAndBLayerLLH_Smooth_v11;   //!
   TBranch        *b_el_isMediumLLH_Smooth_v11;   //!
   TBranch        *b_el_isTightLLH_Smooth_v11;   //!
   TBranch        *b_el_TightLLH_80GeVHighETFixThreshold_Smooth_v11;   //!
   TBranch        *b_el_CFT;   //!
   TBranch        *b_el_passCFT;   //!

   TBranch        *b_el_nBLHits;   //!
   TBranch        *b_el_nBLayerOutliers;   //!
   TBranch        *b_el_expectHitInBLayer;   //!
   TBranch        *b_el_nPixHits;   //!
   TBranch        *b_el_nPixelOutliers;   //!
   TBranch        *b_el_nPixelDeadSensors;   //!
   TBranch        *b_el_nSCTHits;   //!
   TBranch        *b_el_nSCTOutliers;   //!
   TBranch        *b_el_nSCTDeadSensors;   //!
   TBranch        *b_el_nTRTHits;   //!
   TBranch        *b_el_nTRTOutliers;   //!
   TBranch        *b_el_nTRTHighTHits;   //!
   TBranch        *b_el_nTRTHighTOutliers;   //!
   TBranch        *b_el_numberOfTRTDeadStraws;   //!
   TBranch        *b_el_numberOfTRTXenonHits;   //!
   TBranch        *b_el_TRTHighTOutliersRatio;   //!
   TBranch        *b_el_eProbabilityHT;   //!
   TBranch        *b_el_DeltaPOverP;   //!
   TBranch        *b_el_pTErr;   //!
   TBranch        *b_el_deltaCurvOverErrCurv;   //!
   TBranch        *b_el_deltaDeltaPhiFirstAndLM;   //!
   TBranch        *b_el_type;   //!
   TBranch        *b_el_origin;   //!
   TBranch        *b_el_originbkg;   //!
   TBranch        *b_el_isTruthElectronFromZ;   //!
   /* TBranch        *b_el_originbkg;   //! */
   TBranch        *b_el_typebkg;   //!
   TBranch        *b_el_TruthParticlePdgId;   //!
   TBranch        *b_el_firstEgMotherPdgId;   //!
   TBranch        *b_el_TruthParticleBarcode;   //!
   TBranch        *b_el_firstEgMotherBarcode;   //!
   TBranch        *b_el_MotherPdgId;   //!
   TBranch        *b_el_MotherBarcode;   //!
   TBranch        *b_el_FirstEgMotherTyp;   //!
   TBranch        *b_el_FirstEgMotherOrigin;   //!
   TBranch        *b_trig_EF_el_cl_E;   //!
   TBranch        *b_trig_EF_el_etas2;   //!
   TBranch        *b_trig_EF_el_phi;   //!
   TBranch        *b_trig_EF_el_et;   //!
   TBranch        *b_trig_EF_el_pt;   //!
   TBranch        *b_trig_EF_el_charge;   //!
   TBranch        *b_trig_EF_el_Etcone20;   //!
   TBranch        *b_trig_EF_el_Etcone30;   //!
   TBranch        *b_trig_EF_el_Etcone40;   //!
   TBranch        *b_trig_EF_el_ptcone20;   //!
   TBranch        *b_trig_EF_el_E233;   //!
   TBranch        *b_trig_EF_el_E237;   //!
   TBranch        *b_trig_EF_el_E277;   //!
   TBranch        *b_trig_EF_el_Ethad;   //!
   TBranch        *b_trig_EF_el_Ethad1;   //!
   TBranch        *b_trig_EF_el_f1;   //!
   TBranch        *b_trig_EF_el_f3;   //!
   TBranch        *b_trig_EF_el_weta2;   //!
   TBranch        *b_trig_EF_el_wstot;   //!
   TBranch        *b_trig_EF_el_Emins1;   //!
   TBranch        *b_trig_EF_el_emaxs1;   //!
   TBranch        *b_trig_EF_el_Emax2;   //!
   TBranch        *b_trig_EF_el_fracs1;   //!
   TBranch        *b_trig_EF_el_reta;   //!
   TBranch        *b_trig_EF_el_rhad0;   //!
   TBranch        *b_trig_EF_el_rhad1;   //!
   TBranch        *b_trig_EF_el_rhad;   //!
   TBranch        *b_trig_EF_el_rphi;   //!
   TBranch        *b_trig_EF_el_eratio;   //!
   TBranch        *b_trig_EF_el_deltaeta1;   //!
   TBranch        *b_trig_EF_el_deltaphi2;   //!
   TBranch        *b_trig_EF_el_deltaphiRescaled;   //!
   TBranch        *b_trig_EF_el_deltaphiFromLM;   //!
   TBranch        *b_trig_EF_el_trackd0pvunbiased;   //!
   TBranch        *b_trig_EF_el_tracksigd0pvunbiased;   //!
   TBranch        *b_trig_EF_el_d0significance;   //!
   TBranch        *b_trig_EF_el_z0;   //!
   TBranch        *b_trig_EF_el_EOverP;   //!
   TBranch        *b_trig_EF_el_nBLHits;   //!
   TBranch        *b_trig_EF_el_nBLayerOutliers;   //!
   TBranch        *b_trig_EF_el_expectHitInBLayer;   //!
   TBranch        *b_trig_EF_el_nPixHits;   //!
   TBranch        *b_trig_EF_el_nPixelOutliers;   //!
   TBranch        *b_trig_EF_el_nPixelDeadSensors;   //!
   TBranch        *b_trig_EF_el_nSCTHits;   //!
   TBranch        *b_trig_EF_el_nSCTOutliers;   //!
   TBranch        *b_trig_EF_el_nSCTDeadSensors;   //!
   TBranch        *b_trig_EF_el_nTRTHits;   //!
   TBranch        *b_trig_EF_el_nTRTOutliers;   //!
   TBranch        *b_trig_EF_el_nTRTHighTHits;   //!
   TBranch        *b_trig_EF_el_nTRTHighTOutliers;   //!
   TBranch        *b_trig_EF_el_numberOfTRTDeadStraws;   //!
   TBranch        *b_trig_EF_el_numberOfTRTXenonHits;   //!
   TBranch        *b_trig_EF_el_TRTHighTOutliersRatio;   //!
   TBranch        *b_trig_EF_el_eProbabilityHT;   //!
   TBranch        *b_trig_EF_el_DeltaPOverP;   //!
   TBranch        *b_trig_EF_el_pTErr;   //!
   TBranch        *b_trig_EF_el_deltaCurvOverErrCurv;   //!
   TBranch        *b_trig_EF_el_deltaDeltaPhiFirstAndLM;   //!

   vec_removal(TTree *tree=0);
   virtual ~vec_removal();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef vec_removal_cxx
vec_removal::vec_removal(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      /* TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v1_SkimmedNtuple.root/user.kristin.9692952._000001.SkimmedNtuple.root"); */
      /* if (!f || !f->IsOpen()) { */
      /*    f = new TFile("../input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v1_SkimmedNtuple.root/user.kristin.9692952._000001.SkimmedNtuple.root"); */
      /* } */
      /* f->GetObject("tree",tree); */
     //TString files="../input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v4_SkimmedNtuple.root/*.root*"; //v4 without fudging
     TString files="../input/user.kristin.423000.ParticleGun_single_electron_egammaET.e3566_s2726_r7772_r7676.v5_SkimmedNtuple.root/*.root*";//v5 with fudging
     TChain * chain = new TChain("tree");
     chain->Add(files);
     tree = chain;
   }
   Init(tree);
}

vec_removal::~vec_removal()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t vec_removal::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t vec_removal::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void vec_removal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   el_cl_E = 0;
   el_etas2 = 0;
   el_phi = 0;
   el_et = 0;
   el_pt = 0;
   el_charge = 0;
   el_Etcone20 = 0;
   el_Etcone30 = 0;
   el_Etcone40 = 0;
   el_ptcone20 = 0;
   el_E233 = 0;
   el_E237 = 0;
   el_E277 = 0;
   el_Ethad = 0;
   el_Ethad1 = 0;
   el_f1 = 0;
   el_f3 = 0;
   el_weta2 = 0;
   el_wstot = 0;
   el_Emins1 = 0;
   el_emaxs1 = 0;
   el_Emax2 = 0;
   el_fracs1 = 0;
   el_reta = 0;
   el_rhad0 = 0;
   el_rhad1 = 0;
   el_rhad = 0;
   el_rphi = 0;
   el_eratio = 0;
   el_deltaeta1 = 0;
   el_deltaphi2 = 0;
   el_deltaphi1 = 0;
   el_deltaphiRescaled = 0;
   el_deltaphiFromLM = 0;
   el_trackd0pvunbiased = 0;
   el_tracksigd0pvunbiased = 0;
   el_d0significance = 0;
   el_Chi2oftrackmatch = 0;
   /* el_charge = 0; */
   el_z0sinTheta = 0;
   el_z0 = 0;
   el_EOverP = 0;

   el_qoverpsignificance = 0;
   el_isolTight = 0;
   el_isolLoose = 0;
   el_isolLooseTrackOnly = 0;
   el_isolGradient = 0;
   el_isolGradientLoose = 0;
   el_isolFixedCutTightTrackOnly = 0;
   el_FixedCutTight = 0;
   el_FixedCutLoose = 0;
   el_isVeryLooseLL2016_v11 = 0;
   el_isLooseLL2016_v11 = 0;
   el_isLooseAndBLayerLL2016_v11 = 0;
   el_isMediumLL2016_v11 = 0;
   el_isTightLL2016_v11 = 0;
   el_TightLLH_80GeVHighETFixThreshold_v11 = 0;
   el_isVeryLooseLLH_Smooth_v11 = 0;
   el_isLooseLLH_Smooth_v11 = 0;
   el_isLooseAndBLayerLLH_Smooth_v11 = 0;
   el_isMediumLLH_Smooth_v11 = 0;
   el_isTightLLH_Smooth_v11 = 0;
   el_TightLLH_80GeVHighETFixThreshold_Smooth_v11 = 0;
   el_CFT = 0;
   el_passCFT = 0;

   el_nBLHits = 0;
   el_nBLayerOutliers = 0;
   el_expectHitInBLayer = 0;
   el_nPixHits = 0;
   el_nPixelOutliers = 0;
   el_nPixelDeadSensors = 0;
   el_nSCTHits = 0;
   el_nSCTOutliers = 0;
   el_nSCTDeadSensors = 0;
   el_nTRTHits = 0;
   el_nTRTOutliers = 0;
   el_nTRTHighTHits = 0;
   el_nTRTHighTOutliers = 0;
   el_numberOfTRTDeadStraws = 0;
   el_numberOfTRTXenonHits = 0;
   el_TRTHighTOutliersRatio = 0;
   el_eProbabilityHT = 0;
   el_DeltaPOverP = 0;
   el_pTErr = 0;
   el_deltaCurvOverErrCurv = 0;
   el_deltaDeltaPhiFirstAndLM = 0;
   el_type = 0;
   el_origin = 0;
   el_originbkg = 0;
   el_isTruthElectronFromZ = 0;
   /* el_originbkg = 0; */
   el_typebkg = 0;
   el_TruthParticlePdgId = 0;
   el_firstEgMotherPdgId = 0;
   el_TruthParticleBarcode = 0;
   el_firstEgMotherBarcode = 0;
   el_MotherPdgId = 0;
   el_MotherBarcode = 0;
   el_FirstEgMotherTyp = 0;
   el_FirstEgMotherOrigin = 0;
   trig_EF_el_cl_E = 0;
   trig_EF_el_etas2 = 0;
   trig_EF_el_phi = 0;
   trig_EF_el_et = 0;
   trig_EF_el_pt = 0;
   trig_EF_el_charge = 0;
   trig_EF_el_Etcone20 = 0;
   trig_EF_el_Etcone30 = 0;
   trig_EF_el_Etcone40 = 0;
   trig_EF_el_ptcone20 = 0;
   trig_EF_el_E233 = 0;
   trig_EF_el_E237 = 0;
   trig_EF_el_E277 = 0;
   trig_EF_el_Ethad = 0;
   trig_EF_el_Ethad1 = 0;
   trig_EF_el_f1 = 0;
   trig_EF_el_f3 = 0;
   trig_EF_el_weta2 = 0;
   trig_EF_el_wstot = 0;
   trig_EF_el_Emins1 = 0;
   trig_EF_el_emaxs1 = 0;
   trig_EF_el_Emax2 = 0;
   trig_EF_el_fracs1 = 0;
   trig_EF_el_reta = 0;
   trig_EF_el_rhad0 = 0;
   trig_EF_el_rhad1 = 0;
   trig_EF_el_rhad = 0;
   trig_EF_el_rphi = 0;
   trig_EF_el_eratio = 0;
   trig_EF_el_deltaeta1 = 0;
   trig_EF_el_deltaphi2 = 0;
   trig_EF_el_deltaphiRescaled = 0;
   trig_EF_el_deltaphiFromLM = 0;
   trig_EF_el_trackd0pvunbiased = 0;
   trig_EF_el_tracksigd0pvunbiased = 0;
   trig_EF_el_d0significance = 0;
   trig_EF_el_z0 = 0;
   trig_EF_el_EOverP = 0;
   trig_EF_el_nBLHits = 0;
   trig_EF_el_nBLayerOutliers = 0;
   trig_EF_el_expectHitInBLayer = 0;
   trig_EF_el_nPixHits = 0;
   trig_EF_el_nPixelOutliers = 0;
   trig_EF_el_nPixelDeadSensors = 0;
   trig_EF_el_nSCTHits = 0;
   trig_EF_el_nSCTOutliers = 0;
   trig_EF_el_nSCTDeadSensors = 0;
   trig_EF_el_nTRTHits = 0;
   trig_EF_el_nTRTOutliers = 0;
   trig_EF_el_nTRTHighTHits = 0;
   trig_EF_el_nTRTHighTOutliers = 0;
   trig_EF_el_numberOfTRTDeadStraws = 0;
   trig_EF_el_numberOfTRTXenonHits = 0;
   trig_EF_el_TRTHighTOutliersRatio = 0;
   trig_EF_el_eProbabilityHT = 0;
   trig_EF_el_DeltaPOverP = 0;
   trig_EF_el_pTErr = 0;
   trig_EF_el_deltaCurvOverErrCurv = 0;
   trig_EF_el_deltaDeltaPhiFirstAndLM = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Weight", &Weight, &b_Weight);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("Nvtx", &Nvtx, &b_Nvtx);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("el_cl_E", &el_cl_E, &b_el_cl_E);
   fChain->SetBranchAddress("el_etas2", &el_etas2, &b_el_etas2);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_et", &el_et, &b_el_et);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_Etcone20", &el_Etcone20, &b_el_Etcone20);
   fChain->SetBranchAddress("el_Etcone30", &el_Etcone30, &b_el_Etcone30);
   fChain->SetBranchAddress("el_Etcone40", &el_Etcone40, &b_el_Etcone40);
   fChain->SetBranchAddress("el_ptcone20", &el_ptcone20, &b_el_ptcone20);
   fChain->SetBranchAddress("el_E233", &el_E233, &b_el_E233);
   fChain->SetBranchAddress("el_E237", &el_E237, &b_el_E237);
   fChain->SetBranchAddress("el_E277", &el_E277, &b_el_E277);
   fChain->SetBranchAddress("el_Ethad", &el_Ethad, &b_el_Ethad);
   fChain->SetBranchAddress("el_Ethad1", &el_Ethad1, &b_el_Ethad1);
   fChain->SetBranchAddress("el_f1", &el_f1, &b_el_f1);
   fChain->SetBranchAddress("el_f3", &el_f3, &b_el_f3);
   fChain->SetBranchAddress("el_weta2", &el_weta2, &b_el_weta2);
   fChain->SetBranchAddress("el_wstot", &el_wstot, &b_el_wstot);
   fChain->SetBranchAddress("el_Emins1", &el_Emins1, &b_el_Emins1);
   fChain->SetBranchAddress("el_emaxs1", &el_emaxs1, &b_el_emaxs1);
   fChain->SetBranchAddress("el_Emax2", &el_Emax2, &b_el_Emax2);
   fChain->SetBranchAddress("el_fracs1", &el_fracs1, &b_el_fracs1);
   fChain->SetBranchAddress("el_reta", &el_reta, &b_el_reta);
   fChain->SetBranchAddress("el_rhad0", &el_rhad0, &b_el_rhad0);
   fChain->SetBranchAddress("el_rhad1", &el_rhad1, &b_el_rhad1);
   fChain->SetBranchAddress("el_rhad", &el_rhad, &b_el_rhad);
   fChain->SetBranchAddress("el_rphi", &el_rphi, &b_el_rphi);
   fChain->SetBranchAddress("el_eratio", &el_eratio, &b_el_eratio);
   fChain->SetBranchAddress("el_deltaeta1", &el_deltaeta1, &b_el_deltaeta1);
   fChain->SetBranchAddress("el_deltaphi2", &el_deltaphi2, &b_el_deltaphi2);
   fChain->SetBranchAddress("el_deltaphi1", &el_deltaphi1, &b_el_deltaphi1);
   fChain->SetBranchAddress("el_deltaphiRescaled", &el_deltaphiRescaled, &b_el_deltaphiRescaled);
   fChain->SetBranchAddress("el_deltaphiFromLM", &el_deltaphiFromLM, &b_el_deltaphiFromLM);
   fChain->SetBranchAddress("el_trackd0pvunbiased", &el_trackd0pvunbiased, &b_el_trackd0pvunbiased);
   fChain->SetBranchAddress("el_tracksigd0pvunbiased", &el_tracksigd0pvunbiased, &b_el_tracksigd0pvunbiased);
   fChain->SetBranchAddress("el_d0significance", &el_d0significance, &b_el_d0significance);
   fChain->SetBranchAddress("el_Chi2oftrackmatch", &el_Chi2oftrackmatch, &b_el_Chi2oftrackmatch);
//    fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_z0sinTheta", &el_z0sinTheta, &b_el_z0sinTheta);
   fChain->SetBranchAddress("el_z0", &el_z0, &b_el_z0);
   fChain->SetBranchAddress("el_EOverP", &el_EOverP, &b_el_EOverP);

   fChain->SetBranchAddress("el_qoverpsignificance", &el_qoverpsignificance, &b_el_qoverpsignificance);
   fChain->SetBranchAddress("el_isolTight", &el_isolTight, &b_el_isolTight);
   fChain->SetBranchAddress("el_isolLoose", &el_isolLoose, &b_el_isolLoose);
   fChain->SetBranchAddress("el_isolLooseTrackOnly", &el_isolLooseTrackOnly, &b_el_isolLooseTrackOnly);
   fChain->SetBranchAddress("el_isolGradient", &el_isolGradient, &b_el_isolGradient);
   fChain->SetBranchAddress("el_isolGradientLoose", &el_isolGradientLoose, &b_el_isolGradientLoose);
   fChain->SetBranchAddress("el_isolFixedCutTightTrackOnly", &el_isolFixedCutTightTrackOnly, &b_el_isolFixedCutTightTrackOnly);
   fChain->SetBranchAddress("el_FixedCutTight", &el_FixedCutTight, &b_el_FixedCutTight);
   fChain->SetBranchAddress("el_FixedCutLoose", &el_FixedCutLoose, &b_el_FixedCutLoose);
   fChain->SetBranchAddress("el_isVeryLooseLL2016_v11", &el_isVeryLooseLL2016_v11, &b_el_isVeryLooseLL2016_v11);
   fChain->SetBranchAddress("el_isLooseLL2016_v11", &el_isLooseLL2016_v11, &b_el_isLooseLL2016_v11);
   fChain->SetBranchAddress("el_isLooseAndBLayerLL2016_v11", &el_isLooseAndBLayerLL2016_v11, &b_el_isLooseAndBLayerLL2016_v11);
   fChain->SetBranchAddress("el_isMediumLL2016_v11", &el_isMediumLL2016_v11, &b_el_isMediumLL2016_v11);
   fChain->SetBranchAddress("el_isTightLL2016_v11", &el_isTightLL2016_v11, &b_el_isTightLL2016_v11);
   fChain->SetBranchAddress("el_TightLLH_80GeVHighETFixThreshold_v11", &el_TightLLH_80GeVHighETFixThreshold_v11, &b_el_TightLLH_80GeVHighETFixThreshold_v11);
   fChain->SetBranchAddress("el_isVeryLooseLLH_Smooth_v11", &el_isVeryLooseLLH_Smooth_v11, &b_el_isVeryLooseLLH_Smooth_v11);
   fChain->SetBranchAddress("el_isLooseLLH_Smooth_v11", &el_isLooseLLH_Smooth_v11, &b_el_isLooseLLH_Smooth_v11);
   fChain->SetBranchAddress("el_isLooseAndBLayerLLH_Smooth_v11", &el_isLooseAndBLayerLLH_Smooth_v11, &b_el_isLooseAndBLayerLLH_Smooth_v11);
   fChain->SetBranchAddress("el_isMediumLLH_Smooth_v11", &el_isMediumLLH_Smooth_v11, &b_el_isMediumLLH_Smooth_v11);
   fChain->SetBranchAddress("el_isTightLLH_Smooth_v11", &el_isTightLLH_Smooth_v11, &b_el_isTightLLH_Smooth_v11);
   fChain->SetBranchAddress("el_TightLLH_80GeVHighETFixThreshold_Smooth_v11", &el_TightLLH_80GeVHighETFixThreshold_Smooth_v11, &b_el_TightLLH_80GeVHighETFixThreshold_Smooth_v11);
   fChain->SetBranchAddress("el_CFT", &el_CFT, &b_el_CFT);
   fChain->SetBranchAddress("el_passCFT", &el_passCFT, &b_el_passCFT);

   fChain->SetBranchAddress("el_nBLHits", &el_nBLHits, &b_el_nBLHits);
   fChain->SetBranchAddress("el_nBLayerOutliers", &el_nBLayerOutliers, &b_el_nBLayerOutliers);
   fChain->SetBranchAddress("el_expectHitInBLayer", &el_expectHitInBLayer, &b_el_expectHitInBLayer);
   fChain->SetBranchAddress("el_nPixHits", &el_nPixHits, &b_el_nPixHits);
   fChain->SetBranchAddress("el_nPixelOutliers", &el_nPixelOutliers, &b_el_nPixelOutliers);
   fChain->SetBranchAddress("el_nPixelDeadSensors", &el_nPixelDeadSensors, &b_el_nPixelDeadSensors);
   fChain->SetBranchAddress("el_nSCTHits", &el_nSCTHits, &b_el_nSCTHits);
   fChain->SetBranchAddress("el_nSCTOutliers", &el_nSCTOutliers, &b_el_nSCTOutliers);
   fChain->SetBranchAddress("el_nSCTDeadSensors", &el_nSCTDeadSensors, &b_el_nSCTDeadSensors);
   fChain->SetBranchAddress("el_nTRTHits", &el_nTRTHits, &b_el_nTRTHits);
   fChain->SetBranchAddress("el_nTRTOutliers", &el_nTRTOutliers, &b_el_nTRTOutliers);
   fChain->SetBranchAddress("el_nTRTHighTHits", &el_nTRTHighTHits, &b_el_nTRTHighTHits);
   fChain->SetBranchAddress("el_nTRTHighTOutliers", &el_nTRTHighTOutliers, &b_el_nTRTHighTOutliers);
   fChain->SetBranchAddress("el_numberOfTRTDeadStraws", &el_numberOfTRTDeadStraws, &b_el_numberOfTRTDeadStraws);
   fChain->SetBranchAddress("el_numberOfTRTXenonHits", &el_numberOfTRTXenonHits, &b_el_numberOfTRTXenonHits);
   fChain->SetBranchAddress("el_TRTHighTOutliersRatio", &el_TRTHighTOutliersRatio, &b_el_TRTHighTOutliersRatio);
   fChain->SetBranchAddress("el_eProbabilityHT", &el_eProbabilityHT, &b_el_eProbabilityHT);
   fChain->SetBranchAddress("el_DeltaPOverP", &el_DeltaPOverP, &b_el_DeltaPOverP);
   fChain->SetBranchAddress("el_pTErr", &el_pTErr, &b_el_pTErr);
   fChain->SetBranchAddress("el_deltaCurvOverErrCurv", &el_deltaCurvOverErrCurv, &b_el_deltaCurvOverErrCurv);
   fChain->SetBranchAddress("el_deltaDeltaPhiFirstAndLM", &el_deltaDeltaPhiFirstAndLM, &b_el_deltaDeltaPhiFirstAndLM);
   fChain->SetBranchAddress("el_type", &el_type, &b_el_type);
   fChain->SetBranchAddress("el_origin", &el_origin, &b_el_origin);
   fChain->SetBranchAddress("el_originbkg", &el_originbkg, &b_el_originbkg);
   fChain->SetBranchAddress("el_isTruthElectronFromZ", &el_isTruthElectronFromZ, &b_el_isTruthElectronFromZ);
//    fChain->SetBranchAddress("el_originbkg", &el_originbkg, &b_el_originbkg);
   fChain->SetBranchAddress("el_typebkg", &el_typebkg, &b_el_typebkg);
   fChain->SetBranchAddress("el_TruthParticlePdgId", &el_TruthParticlePdgId, &b_el_TruthParticlePdgId);
   fChain->SetBranchAddress("el_firstEgMotherPdgId", &el_firstEgMotherPdgId, &b_el_firstEgMotherPdgId);
   fChain->SetBranchAddress("el_TruthParticleBarcode", &el_TruthParticleBarcode, &b_el_TruthParticleBarcode);
   fChain->SetBranchAddress("el_firstEgMotherBarcode", &el_firstEgMotherBarcode, &b_el_firstEgMotherBarcode);
   fChain->SetBranchAddress("el_MotherPdgId", &el_MotherPdgId, &b_el_MotherPdgId);
   fChain->SetBranchAddress("el_MotherBarcode", &el_MotherBarcode, &b_el_MotherBarcode);
   fChain->SetBranchAddress("el_FirstEgMotherTyp", &el_FirstEgMotherTyp, &b_el_FirstEgMotherTyp);
   fChain->SetBranchAddress("el_FirstEgMotherOrigin", &el_FirstEgMotherOrigin, &b_el_FirstEgMotherOrigin);
   fChain->SetBranchAddress("trig_EF_el_cl_E", &trig_EF_el_cl_E, &b_trig_EF_el_cl_E);
   fChain->SetBranchAddress("trig_EF_el_etas2", &trig_EF_el_etas2, &b_trig_EF_el_etas2);
   fChain->SetBranchAddress("trig_EF_el_phi", &trig_EF_el_phi, &b_trig_EF_el_phi);
   fChain->SetBranchAddress("trig_EF_el_et", &trig_EF_el_et, &b_trig_EF_el_et);
   fChain->SetBranchAddress("trig_EF_el_pt", &trig_EF_el_pt, &b_trig_EF_el_pt);
   fChain->SetBranchAddress("trig_EF_el_charge", &trig_EF_el_charge, &b_trig_EF_el_charge);
   fChain->SetBranchAddress("trig_EF_el_Etcone20", &trig_EF_el_Etcone20, &b_trig_EF_el_Etcone20);
   fChain->SetBranchAddress("trig_EF_el_Etcone30", &trig_EF_el_Etcone30, &b_trig_EF_el_Etcone30);
   fChain->SetBranchAddress("trig_EF_el_Etcone40", &trig_EF_el_Etcone40, &b_trig_EF_el_Etcone40);
   fChain->SetBranchAddress("trig_EF_el_ptcone20", &trig_EF_el_ptcone20, &b_trig_EF_el_ptcone20);
   fChain->SetBranchAddress("trig_EF_el_E233", &trig_EF_el_E233, &b_trig_EF_el_E233);
   fChain->SetBranchAddress("trig_EF_el_E237", &trig_EF_el_E237, &b_trig_EF_el_E237);
   fChain->SetBranchAddress("trig_EF_el_E277", &trig_EF_el_E277, &b_trig_EF_el_E277);
   fChain->SetBranchAddress("trig_EF_el_Ethad", &trig_EF_el_Ethad, &b_trig_EF_el_Ethad);
   fChain->SetBranchAddress("trig_EF_el_Ethad1", &trig_EF_el_Ethad1, &b_trig_EF_el_Ethad1);
   fChain->SetBranchAddress("trig_EF_el_f1", &trig_EF_el_f1, &b_trig_EF_el_f1);
   fChain->SetBranchAddress("trig_EF_el_f3", &trig_EF_el_f3, &b_trig_EF_el_f3);
   fChain->SetBranchAddress("trig_EF_el_weta2", &trig_EF_el_weta2, &b_trig_EF_el_weta2);
   fChain->SetBranchAddress("trig_EF_el_wstot", &trig_EF_el_wstot, &b_trig_EF_el_wstot);
   fChain->SetBranchAddress("trig_EF_el_Emins1", &trig_EF_el_Emins1, &b_trig_EF_el_Emins1);
   fChain->SetBranchAddress("trig_EF_el_emaxs1", &trig_EF_el_emaxs1, &b_trig_EF_el_emaxs1);
   fChain->SetBranchAddress("trig_EF_el_Emax2", &trig_EF_el_Emax2, &b_trig_EF_el_Emax2);
   fChain->SetBranchAddress("trig_EF_el_fracs1", &trig_EF_el_fracs1, &b_trig_EF_el_fracs1);
   fChain->SetBranchAddress("trig_EF_el_reta", &trig_EF_el_reta, &b_trig_EF_el_reta);
   fChain->SetBranchAddress("trig_EF_el_rhad0", &trig_EF_el_rhad0, &b_trig_EF_el_rhad0);
   fChain->SetBranchAddress("trig_EF_el_rhad1", &trig_EF_el_rhad1, &b_trig_EF_el_rhad1);
   fChain->SetBranchAddress("trig_EF_el_rhad", &trig_EF_el_rhad, &b_trig_EF_el_rhad);
   fChain->SetBranchAddress("trig_EF_el_rphi", &trig_EF_el_rphi, &b_trig_EF_el_rphi);
   fChain->SetBranchAddress("trig_EF_el_eratio", &trig_EF_el_eratio, &b_trig_EF_el_eratio);
   fChain->SetBranchAddress("trig_EF_el_deltaeta1", &trig_EF_el_deltaeta1, &b_trig_EF_el_deltaeta1);
   fChain->SetBranchAddress("trig_EF_el_deltaphi2", &trig_EF_el_deltaphi2, &b_trig_EF_el_deltaphi2);
   fChain->SetBranchAddress("trig_EF_el_deltaphiRescaled", &trig_EF_el_deltaphiRescaled, &b_trig_EF_el_deltaphiRescaled);
   fChain->SetBranchAddress("trig_EF_el_deltaphiFromLM", &trig_EF_el_deltaphiFromLM, &b_trig_EF_el_deltaphiFromLM);
   fChain->SetBranchAddress("trig_EF_el_trackd0pvunbiased", &trig_EF_el_trackd0pvunbiased, &b_trig_EF_el_trackd0pvunbiased);
   fChain->SetBranchAddress("trig_EF_el_tracksigd0pvunbiased", &trig_EF_el_tracksigd0pvunbiased, &b_trig_EF_el_tracksigd0pvunbiased);
   fChain->SetBranchAddress("trig_EF_el_d0significance", &trig_EF_el_d0significance, &b_trig_EF_el_d0significance);
   fChain->SetBranchAddress("trig_EF_el_z0", &trig_EF_el_z0, &b_trig_EF_el_z0);
   fChain->SetBranchAddress("trig_EF_el_EOverP", &trig_EF_el_EOverP, &b_trig_EF_el_EOverP);
   fChain->SetBranchAddress("trig_EF_el_nBLHits", &trig_EF_el_nBLHits, &b_trig_EF_el_nBLHits);
   fChain->SetBranchAddress("trig_EF_el_nBLayerOutliers", &trig_EF_el_nBLayerOutliers, &b_trig_EF_el_nBLayerOutliers);
   fChain->SetBranchAddress("trig_EF_el_expectHitInBLayer", &trig_EF_el_expectHitInBLayer, &b_trig_EF_el_expectHitInBLayer);
   fChain->SetBranchAddress("trig_EF_el_nPixHits", &trig_EF_el_nPixHits, &b_trig_EF_el_nPixHits);
   fChain->SetBranchAddress("trig_EF_el_nPixelOutliers", &trig_EF_el_nPixelOutliers, &b_trig_EF_el_nPixelOutliers);
   fChain->SetBranchAddress("trig_EF_el_nPixelDeadSensors", &trig_EF_el_nPixelDeadSensors, &b_trig_EF_el_nPixelDeadSensors);
   fChain->SetBranchAddress("trig_EF_el_nSCTHits", &trig_EF_el_nSCTHits, &b_trig_EF_el_nSCTHits);
   fChain->SetBranchAddress("trig_EF_el_nSCTOutliers", &trig_EF_el_nSCTOutliers, &b_trig_EF_el_nSCTOutliers);
   fChain->SetBranchAddress("trig_EF_el_nSCTDeadSensors", &trig_EF_el_nSCTDeadSensors, &b_trig_EF_el_nSCTDeadSensors);
   fChain->SetBranchAddress("trig_EF_el_nTRTHits", &trig_EF_el_nTRTHits, &b_trig_EF_el_nTRTHits);
   fChain->SetBranchAddress("trig_EF_el_nTRTOutliers", &trig_EF_el_nTRTOutliers, &b_trig_EF_el_nTRTOutliers);
   fChain->SetBranchAddress("trig_EF_el_nTRTHighTHits", &trig_EF_el_nTRTHighTHits, &b_trig_EF_el_nTRTHighTHits);
   fChain->SetBranchAddress("trig_EF_el_nTRTHighTOutliers", &trig_EF_el_nTRTHighTOutliers, &b_trig_EF_el_nTRTHighTOutliers);
   fChain->SetBranchAddress("trig_EF_el_numberOfTRTDeadStraws", &trig_EF_el_numberOfTRTDeadStraws, &b_trig_EF_el_numberOfTRTDeadStraws);
   fChain->SetBranchAddress("trig_EF_el_numberOfTRTXenonHits", &trig_EF_el_numberOfTRTXenonHits, &b_trig_EF_el_numberOfTRTXenonHits);
   fChain->SetBranchAddress("trig_EF_el_TRTHighTOutliersRatio", &trig_EF_el_TRTHighTOutliersRatio, &b_trig_EF_el_TRTHighTOutliersRatio);
   fChain->SetBranchAddress("trig_EF_el_eProbabilityHT", &trig_EF_el_eProbabilityHT, &b_trig_EF_el_eProbabilityHT);
   fChain->SetBranchAddress("trig_EF_el_DeltaPOverP", &trig_EF_el_DeltaPOverP, &b_trig_EF_el_DeltaPOverP);
   fChain->SetBranchAddress("trig_EF_el_pTErr", &trig_EF_el_pTErr, &b_trig_EF_el_pTErr);
   fChain->SetBranchAddress("trig_EF_el_deltaCurvOverErrCurv", &trig_EF_el_deltaCurvOverErrCurv, &b_trig_EF_el_deltaCurvOverErrCurv);
   fChain->SetBranchAddress("trig_EF_el_deltaDeltaPhiFirstAndLM", &trig_EF_el_deltaDeltaPhiFirstAndLM, &b_trig_EF_el_deltaDeltaPhiFirstAndLM);
   Notify();
}

Bool_t vec_removal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void vec_removal::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t vec_removal::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef vec_removal_cxx
