#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TString.h"
#include "TCut.h"
#include "TTree.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TROOT.h"

void bfr_aft (TString var="abs(eta)") {
  gStyle->SetOptStat(0);

  //TString trainName = "pid3_etaBin0_pe2s_t100_d5_c200_m2"; float op95 = -0.0581835;
  //TString trainName= "pid3_etaBin0_t100_d5_c200_m2";       float op95 = -0.0687003;

  //KM: for weight comparison without (pt,eta) in input
  //TString trainName= "pid3_etaBin0_t100_d5_c200_m2";       float op95 = -0.0619391;
  //TString trainName= "pid3_etaBin0_pe2b_t100_d5_c200_m2";  float op95 = -0.0528371;
  //TString trainName= "pid3_etaBin0_pe2s_t100_d5_c200_m2";  float op95 = -0.0637269;
  TString trainName= "pid3_etaBin0_pef_t100_d5_c200_m2";   float op95 = -0.0846174;

  //KM: for weight comparison with (pt,eta) in input
  //TString trainName= "pid3_etaBin0_t100_d5_c200_m2_p";       float op95 = -0.0173294;
  //TString trainName= "pid3_etaBin0_pe2b_t100_d5_c200_m2_p";  float op95 = -0.0345989;
  //TString trainName= "pid3_etaBin0_pe2s_t100_d5_c200_m2_p";  float op95 = -0.0472328;
  //TString trainName= "pid3_etaBin0_pef_t100_d5_c200_m2_p";   float op95 = -0.0372803;

  TString file= "postprocess/TMVA_"+trainName+".root";
  TFile* f = TFile::Open(file,"read");
  TTree* TestTree = (TTree*) f->Get("TestTree");

  const TString bdt_name = "BDT_"+trainName;
  TCut op_cut = (TCut)(bdt_name + TString::Format(">%f",op95));

  TH1F* h_sig_bfr = new TH1F("h_sig_bfr",trainName,50, 0, 2.5);
  TH1F* h_sig_aft = new TH1F("h_sig_aft",trainName,50, 0, 2.5);
  TH1F* h_bkg_bfr = new TH1F("h_bkg_bfr",trainName,50, 0, 2.5);
  TH1F* h_bkg_aft = new TH1F("h_bkg_aft",trainName,50, 0, 2.5);

  TestTree->Project(h_sig_bfr->GetName(), var,"classID==0"       , "");
  TestTree->Project(h_sig_aft->GetName(), var,"classID==0"+op_cut, "");
  TestTree->Project(h_bkg_bfr->GetName(), var,"classID==1"       , "");
  TestTree->Project(h_bkg_aft->GetName(), var,"classID==1"+op_cut, "");

  float norm_sig = h_sig_bfr->Integral();
  float norm_bkg = h_bkg_bfr->Integral();

  h_sig_bfr->Scale(1/norm_sig);
  h_sig_aft->Scale(1/norm_sig);
  h_bkg_bfr->Scale(1/norm_bkg);
  h_bkg_aft->Scale(1/norm_bkg);

  h_bkg_bfr->SetLineColor(kRed);  h_bkg_bfr->SetLineStyle(2);
  h_bkg_aft->SetLineColor(kRed);
  h_sig_bfr->SetLineColor(kBlue); h_sig_bfr->SetLineStyle(2);
  h_sig_aft->SetLineColor(kBlue);

  h_bkg_bfr->Draw("");
  h_bkg_aft->Draw("same");
  h_sig_bfr->Draw("same");
  h_sig_aft->Draw("same");

  TCanvas *c1 = (TCanvas*)gROOT->FindObject("c1");
  c1->SaveAs(trainName+".png");

}
